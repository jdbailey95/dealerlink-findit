﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="FrontEnd.Main" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>SkyLINK</title>
    <link rel="shortcut icon" href="images/favicon.ico" />

	<link rel="apple-touch-icon" href="images/touch-icon-iphone.png" />
	<link rel="apple-touch-icon" sizes="72x72" href="images/touch-icon-ipad.png" />
	<link rel="apple-touch-icon" sizes="114x114" href="images/touch-icon-iphone4.png" />
    
	<link rel="stylesheet" type="text/css" href="mobilePROTECT.css" />

    <meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
</head>

<body>
   	<!--#include file="banner.inc" -->
    <form id="form1" runat="server">
    <ajx:ToolkitScriptManager runat="server" ID="scriptManager" />
    <div id="loginContainer">

    <!-- TODO: Get rid of this ugly table -->
    	<table style="margin-left:auto; margin-right:auto">
    		<tr><td style='text-align:right; width:1px;'>Email: </td>
    		  <td><div id="SLCurMidLftEmlVal" >
                    <asp:TextBox runat="server" ID="txtEmail" />
                  </div></td></tr>
            <tr><td style='text-align:right;width:1px;'>Password: </td>
              <td><div id="SLCurMidLftPWVal">
                <asp:TextBox runat="server" ID="txtPwd" TextMode="Password" />
                </div>
              </td>
            </tr>
          </table>
    <!-- TODO: Get rid of this ugly table -->
        </div>
	    <div runat="server" id="divError">
		  <asp:Label runat="server" ID="lblError" Text="" style="color:Red;font-size:medium;"/><br />
		</div>
        <div id="SLCurMidRtDIV">
            <div id="SLCurMidRtBotBut">
				<asp:ImageButton runat="server" ID="robLogin"
					ImageUrl="images/btn_login.png"
					CausesValidation="true" OnClick="robLogin_Click" />

                <%--<pkp:RollOverButton runat="server" ID="robLogin" 
                    ImageUrl="images/btn_login.png"
                    ImageOverUrl="images/btn_login.png"
                    CausesValidation="true"
                    OnClick="robLogin_Click" />--%>
            </div>
        </div>
        <asp:RequiredFieldValidator runat="server" ID="rfvEmail" InitialValue="" Display="None"
            ControlToValidate="txtEmail" ErrorMessage="email address required" />
        <ajx:ValidatorCalloutExtender runat="server" ID="vceEmail" PopupPosition="Right"
            TargetControlID="rfvEmail" />
        <asp:RegularExpressionValidator runat="server" ID="regExpValEmail" ErrorMessage="invalid email format"
            ControlToValidate="txtEmail" ValidationExpression="^(([A-Za-z0-9]+_+)|([A-Za-z0-9]+\-+)|([A-Za-z0-9]+\.+)|([A-Za-z0-9]+\++))*[A-Za-z0-9]+@((\w+\-+)|(\w+\.))*\w{1,63}\.[a-zA-Z]{2,6}$"
            Display="None" />
        <ajx:ValidatorCalloutExtender runat="server" PopupPosition="right" TargetControlID="regExpValEmail"
            ID="ValidatorCalloutExtender6" />
        <%--<asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" InitialValue=""
            Display="None" ControlToValidate="txtPwd" ErrorMessage="password required" />
        <ajx:ValidatorCalloutExtender runat="server" ID="ValidatorCalloutExtender1" PopupPosition="right"
            TargetControlID="RequiredFieldValidator1" />--%>
            <asp:HiddenField ID="hdVIN" runat="server" />
            <asp:HiddenField ID="hdHash" runat="server" />
            <asp:HiddenField ID="hdEpoch" runat="server" />
            <asp:HiddenField ID="hdTicks" runat="server" />
    </div>
    </form>
    <a href="ResendPassword.aspx">Forgot password?</a>
</body>
</html>
