﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
//using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Diagnostics;
using System.Text;
using System.Security.Cryptography;
using System.Web.UI;
using System.Web.SessionState;

using VehicleManagement.Data;

namespace SkyLink
{

    /// <summary>
    /// Summary description for SkylinkUtil
    /// </summary>
    public static class SkylinkUtil
    {
        private static string _salt = "Forty-four Americans have now taken the presidential oath.  The words have been spoken during rising tides of prosperity and the still waters of peace.  Yet, every so often, the oath is taken amidst gathering clouds and raging storms.  At these moments, America has carried on not simply because of the skill or vision of those in high office, but because we, the people, have remained faithful to the ideals of our forebears and true to our founding documents. PuppyUnicornsRandomTreeHouse";
        private static int daysEmailIsValid = 5;

        public static int DaysEmailIsValid { get { return daysEmailIsValid; } }

        public static int? UserID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(System.Web.HttpContext.Current.Session["UserID"]);
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                System.Web.HttpContext.Current.Session["UserID"] = value;
            }
        }

        public static int? AltUserID
        {
            get
            {
                try
                {
                    return Convert.ToInt32(System.Web.HttpContext.Current.Session["AltUserID"]);
                }
                catch
                {
                    return 0;
                }
            }
            set
            {
                System.Web.HttpContext.Current.Session["AltUserID"] = value;
            }
        }

        public static Boolean IsAdvantageUser
        {
            get
            {
                return (bool)System.Web.HttpContext.Current.Session["IsAdvantageUser"];
            }
            set
            {
                System.Web.HttpContext.Current.Session["IsAdvantageUser"] = value;
            }
        }

        public static String UserVIN
        {
            get
            {
                try
                {
                    return System.Web.HttpContext.Current.Session["UserVIN"].ToString();
                }
                catch
                {
                    return null;
                }
            }

            set
            {
                System.Web.HttpContext.Current.Session["UserVIN"] = value;
            }
        }

        public static IQueryable<Vehicle> UserVehicles
        {
            get
            {
                return (IQueryable<Vehicle>)System.Web.HttpContext.Current.Session["UserVehicles"];
            }

            set
            {
                System.Web.HttpContext.Current.Session["UserVehicles"] = value;
            }
        }
        /// <summary>
        /// Returns the UserID of the supplied credentials
        /// or 0 if the login was invalid.
        /// Added setting the userID session and available Vehicle in the session
        /// </summary>
        /// <param name="email">the email address of the user</param>
        /// <param name="pwd">plain text of the user</param>
        /// <returns></returns>
        public static int Login_SkyLinkUser(String email, String pwd)
        {
            using (var db = new VehicleManagementDataContext())
            {
                int? uid = null;
                int ret = db.LoginUser(email, pwd, ref uid);
                if (ret != 0)
                {
                    return 0;
                }

                if (uid.HasValue)
                {
                    UserID = uid.Value;

                    // If this is a support user, default to logging in as that user - thus providing
                    // the option to override and login as a different, alternate user.
                    if (db.SupportUsers.FirstOrDefault(z => z.ID == uid.Value) != null)
                    {
                        AltUserID = uid.Value;  // Default alternate user id to the support user.
                    }
                    else
                    {
                        AltUserID = 0;  // Not a support user, so disallow alternate user login.
                    }

                    //UserVehicles = db.ConsumerUsers.First(z => z.ID == UserID).Account.Vehicles;

                    //IsAdvantageUser = db.Ve

                    return uid.Value;
                }
                else
                {
                    return 0;
                }
            }
        }


        /// <summary>
        /// Generates a random string of the specified length.
        /// </summary>
        /// <param name="size"></param>
        /// <returns></returns>
        public static string RandomString(int size)
        {
            Random _random = new Random();

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < size; i++)
            {

                //26 letters in the alfabet, ascii + 65 for the capital letters
                builder.Append(Convert.ToChar(Convert.ToInt32(
                    Math.Floor(26 * _random.NextDouble() + 65))));

            }
            return builder.ToString();
        }

        public static void FillStates(this DropDownList listBox)
        {
            ListItemCollection items = listBox.Items;
            items.Clear();
            items.Add("AL");
            items.Add("AK");
            items.Add("AS");
            items.Add("AZ");
            items.Add("AR");
            items.Add("CA");
            items.Add("CO");
            items.Add("CT");
            items.Add("DE");
            items.Add("DC");
            items.Add("FM");
            items.Add("FL");
            items.Add("GA");
            items.Add("HI");
            items.Add("ID");
            items.Add("IL");
            items.Add("IN");
            items.Add("IA");
            items.Add("KS");
            items.Add("KY");
            items.Add("LA");
            items.Add("ME");
            items.Add("MH");
            items.Add("MD");
            items.Add("MA");
            items.Add("MI");
            items.Add("MN");
            items.Add("MS");
            items.Add("MO");
            items.Add("MT");
            items.Add("NE");
            items.Add("NV");
            items.Add("NH");
            items.Add("NJ");
            items.Add("NM");
            items.Add("NY");
            items.Add("NC");
            items.Add("ND");
            items.Add("OH");
            items.Add("OK");
            items.Add("OR");
            items.Add("PA");
            items.Add("PR");
            items.Add("RI");
            items.Add("SC");
            items.Add("SD");
            items.Add("TN");
            items.Add("TX");
            items.Add("UT");
            items.Add("VT");
            items.Add("VI");
            items.Add("VA");
            items.Add("WA");
            items.Add("WV");
            items.Add("WI");
            items.Add("WY");
        }

        public static void FillCellPhoneProviders(this DropDownList listBox, String selectedValue = "")
        {
            ListItemCollection items = listBox.Items;
            items.Clear();

            var db = new VehicleManagementDataContext();
            var cellProviders = db.CellPhoneProviders.Select(cpp => new { display = cpp.Name, value = cpp.ID });

            listBox.DataSource = cellProviders;
            listBox.DataTextField = "display";
            listBox.DataValueField = "value";
            listBox.DataBind();

            if (selectedValue != "")
                listBox.SelectedValue = selectedValue;
        }

        public static void FillSecretQuestions(this DropDownList listBox)
        {
            ListItemCollection items = listBox.Items;
            items.Clear();

            var db = new VehicleManagementDataContext();
            var cellProviders = db.SecretQuestions.Select(cpp => new { display = cpp.Question, value = cpp.ID });

            listBox.DataSource = cellProviders;
            listBox.DataTextField = "display";
            listBox.DataValueField = "value";
            listBox.DataBind();
        }

        /// <summary>
        /// Build the link that will be used to validate a new user
        /// </summary>
        /// <param name="email"></param>
        /// <param name="vin"></param>
        /// <returns></returns>
        public static String EncodeLinkForNewUserActivation(String email, String vin)
        {
            //constants
            string urlRoot = "https://mysky-link.com/CreateAccount.aspx";
            DateTime epoch = DateTime.UtcNow;

            //get ticks
            long urlTicks = (DateTime.UtcNow - epoch).Ticks / TimeSpan.TicksPerHour;

            //get hash
            SHA1 sha = SHA1.Create();
            byte[] clearText = Encoding.UTF8.GetBytes(_salt + email + vin + urlTicks.ToString() + epoch.ToString("MM/dd/yyyy"));
            byte[] rawHash = sha.ComputeHash(clearText);
            string hash = HttpServerUtility.UrlTokenEncode(rawHash);

            //create URL
            String url = string.Format("{0}?email={1}&vin={2}&ts={3}&hs={4}&dt={5}", urlRoot, email, vin, urlTicks, hash, epoch.ToString("MM/dd/yyyy"));

            return url;
        }

        public static Boolean DecodeLinkForNewUserActivation(String email, String vin, String tmpurlTicks, String hash, String tmpepoch)
        {
            DateTime epoch = DateTime.Now.AddYears(-99);
            long urlTicks = 99999999;

            //constants
            TimeSpan maxAge = TimeSpan.FromDays(daysEmailIsValid);

            try //If the url is malformed and the values cannot be correctly converted catch it
            {
                epoch = Convert.ToDateTime(tmpepoch);
                urlTicks = Convert.ToInt64(tmpurlTicks);
            }
            catch
            {
                return false;
            }

            //make hash
            SHA1 comparesha = SHA1.Create();
            byte[] compareClearText = Encoding.UTF8.GetBytes(_salt + email + vin + urlTicks.ToString() + epoch.ToString("MM/dd/yyyy"));
            byte[] compareRawHash = comparesha.ComputeHash(compareClearText);
            string compareHash = HttpServerUtility.UrlTokenEncode(compareRawHash);

            //compare hash
            if (compareHash != hash)
            {
                //"Hash does not match - the URL is not valid".Dump("HASH check");
                return false;
            }
            //"Hash matches".Dump("HASH check");

            //compute age of URL
            TimeSpan age = DateTime.UtcNow - epoch.AddHours(urlTicks);
            if (age > maxAge)
            {
                //"URL too old".Dump("Age check");
                return false;
            }

            return true;
            //("URL age passes with value " + age.ToString()).Dump("Age Check");
        }

        public static String GetCreateUserErrorDescription(int errorNumber)
        {
            String description = null;

            switch (errorNumber)
            {
                case -1:
                    description = "The First Name field cannot be empty";
                    break;
                case -2:
                    description = "The Last Name field cannot be empty";
                    break;
                case -3:
                    description = "The E-mail field cannot be empty";
                    break;
                case -4:
                    description = "The Cell Phone Provider field cannot be empty when a Cell Phone number has been provied";
                    break;
                case -5:
                    description = "The Cell Phone Provider field cannot be empty when a Cell Phone number has been provied";
                    break;
                case -6:
                    description = "The Password field cannot be empty";
                    break;
                case -7:
                    description = "The User Type of this account is not set";
                    break;
                case -8:
                    description = "The Timezone field cannot be empty";
                    break;
                case -9:
                    description = "The Daylight Savings Time field cannot be empty";
                    break;
                case -10:
                    description = "The Account ID could not be created or is empty";
                    break;
                case -11:
                    description = "The Authorization of this account cannot be empty";
                    break;
                case -12:
                    description = "There are no Allowed Notifications set";
                    break;
                case -17:
                    description = "A e-mail address shorter than 5 characters cannot be used";
                    break;
                case -18:
                    description = "The password must be at least 5 characters in length";
                    break;
                case -32:
                    description = "Unable to create the account requested at this time";
                    break;
                case -34:
                    description = "The Cell Provider entered does not exist";
                    break;
                case -48:
                    description = "The e-mail addressed entered already exists in our system. ";
                    break;
                default:
                    description = "";
                    break;
            }

            return description;
        }
    }
}