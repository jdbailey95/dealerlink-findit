﻿using System.Runtime.Serialization;
using VehicleManagement.Data;

namespace SkyLinkWCFService.DataTypes
{
	/// <summary>
	/// Types of location alarms
	/// </summary>
	[DataContract]
	public enum LocationAlarmType
	{
		/// <summary>
		/// No alarm
		/// </summary>
		[EnumMember]
		None = (int)LocationAlertType.None,

		/// <summary>
		/// Ignition on alarm
		/// </summary>
		[EnumMember]
		IgnitionOn = (int)LocationAlertType.IgnitionOn,

		/// <summary>
		/// Ignition off alarm
		/// </summary>
		[EnumMember]
		IgnitionOff = (int)LocationAlertType.IgnitionOff,

		/// <summary>
		/// Geofence enter alarm
		/// </summary>
		[EnumMember]
		GeofenceEnter = (int)LocationAlertType.GeofenceEnter,

		/// <summary>
		/// Geofence exit alarm
		/// </summary>
		[EnumMember]
		GeofenceExit = (int)LocationAlertType.GeofenceExit,

		/// <summary>
		/// ETD alarm
		/// </summary>
		[EnumMember]
		EarlyTheftDetection = (int)LocationAlertType.EarlyTheftDetection,

		/// <summary>
		/// Overspeed alarm
		/// </summary>
		[EnumMember]
		OverSpeed = (int)LocationAlertType.OverSpeed,

		/// <summary>
		/// Low battery level alarm
		/// </summary>
		[EnumMember]
		LowBatteryLevel = (int)LocationAlertType.LowBatteryLevel,

		/// <summary>
		/// Power disconnect alarm
		/// </summary>
		[EnumMember]
		PowerDisconnect = (int)LocationAlertType.PowerDisconnect,

    	/// <summary>
		/// Power reconnect alarm
		/// </summary>
		[EnumMember]
		PowerReconnect = (int)LocationAlertType.PowerReconnect,

        /// <summary>
		/// Idle alarm
		/// </summary>
		[EnumMember]
		Idle = (int)LocationAlertType.Idle
}
}