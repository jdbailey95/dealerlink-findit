﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    public class DashBoardInfo
    {
        private string _currentVin;

        public DashBoardInfo(string vin)
        {
            _currentVin = vin;
        }

        /// <summary>
        /// Gets or sets the latest trips summary.
        /// </summary>
        /// <value>The latest trips summary.</value>
        [DataMember]
        public IEnumerable<TripSummary> LatestTripsSummary { get; set; }

        /// <summary>
        /// Gets or sets the user infomation.
        /// </summary>
        /// <value>The user infomation.</value>
        [DataMember]
        public SkyLinkUserInformation CurrentUserInfomation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [quick fence set].
        /// </summary>
        /// <value><c>true</c> if [quick fence set]; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsQuickFenceSet { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [quick fence violated].
        /// </summary>
        /// <value><c>true</c> if [quick fence violated]; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsQuickFenceViolated { get; set; }

        /// <summary>
        /// Gets or sets the speed alert value.
        /// </summary>
        /// <value>The speed alert value.</value>
        [DataMember]
        public int? SpeedAlertValue { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [speed alert exceeded].
        /// </summary>
        /// <value><c>true</c> if [speed alert exceeded]; otherwise, <c>false</c>.</value>
        [DataMember]
        public bool IsSpeedAlarmed { get; set; }

        /// <summary>
        /// Gets or sets the battery level.
        /// </summary>
        /// <value>The battery level.</value>
        [DataMember]
        public float? BatteryLevelSetting { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is battery alarmed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance is battery alarmed; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool IsBatteryAlarmed { get; set; }

        /// <summary>
        /// Gets or sets the geo fences.
        /// </summary>
        /// <value>The geo fences.</value>
        [DataMember]
        public IEnumerable<Geofence> GeoFences { get; set; }

        /// <summary>
        /// Gets or sets the supported fence types.
        /// </summary>
        /// <value>The supported fence types.</value>
        [DataMember]
        public IEnumerable<EFenceGeometery> SupportedFenceTypes { get; set; }

        /// <summary>
        /// Gets or sets the number of geo fences supported.
        /// </summary>
        /// <value>The number of geo fences supported.</value>
        [DataMember]
        public int NumberOfGeofencesSupported { get; set; }

        /// <summary>
        /// Gets or sets the insurance card.
        /// </summary>
        /// <value>The insurance card.</value>
        [DataMember]
        public InsuranceCard InsuranceCard { get; set; }

        /// <summary>
        /// Gets or sets the most recent location.
        /// </summary>
        /// <value>The most recent location.</value>
        [DataMember]
        public LocationPoint LastLocation { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has battery alerts enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has battery alerts enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool BatteryAlertEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether this instance has speed alerts enabled.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has speed alerts enabled; otherwise, <c>false</c>.
        /// </value>
        [DataMember]
        public bool SpeedAlertEnabled { get; set; }

    }
}