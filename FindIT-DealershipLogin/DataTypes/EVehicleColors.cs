﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    [Obsolete("not sure if we want to use an enum here.")]
    public enum EVehicleColors
    {
        [EnumMember]
        BEIGE = 1,
        [EnumMember]
        BLACK,
        [EnumMember]
        BLUE,
        [EnumMember]
        BROWN,
        [EnumMember]
        BURGUNDY,
        [EnumMember]
        CHARCOAL,
        [EnumMember]
        GOLD,
        [EnumMember]
        GRAY,
        [EnumMember]
        GREEN,
        [EnumMember]
        OFFWHITE,
        [EnumMember]
        ORANGE,
        [EnumMember]
        PINK,
        [EnumMember]
        PURPLE,
        [EnumMember]
        RED,
        [EnumMember]
        SILVER,
        [EnumMember]
        TAN,
        [EnumMember]
        TURQUOISE,
        [EnumMember]
        WHITE,
        [EnumMember]
        YELLOW
    }
}