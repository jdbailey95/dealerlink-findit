﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	[DataContract]
	public class AllProducts
	{
		[DataMember]
		public IEnumerable<ProductOptionGroup> ProductGroups;
	}

	[DataContract]
	public class ProductOptionGroup
	{
		[DataMember]
		public string GroupName; //ie Subscription plans, hats ect..
		[DataMember]
		public IEnumerable<Product> LineItems;
		[DataMember]
		public bool IsListType;
	}

	[DataContract]
	public class Product
	{
		[DataMember]
		public int ID; //what makes this unique
		[DataMember]
		public int OrdinalNumber; //it's position within the group 1year before 2 year ect
		[DataMember]
		public string Name; //Advantage 1 year or Advantage 2 year or SkyLink Hat
		[DataMember]
		public string Description;//tells them what they are buying
		[DataMember]
		public Decimal Price; //how much money they have to give us to get this
		[DataMember]
		public string ImageUrl; //if we have an image for the product
		[DataMember]
		public CartItemType Type; //what kind of item it is
	}

	/// <summary>
	/// This object is returned by the Client, it contains which items they've choosen
	/// to give us money for.
	/// </summary>
	[DataContract]
	public class Cart
	{
		[DataMember]
		public string VIN;
		[DataMember]
		public IEnumerable<Product> Items; // the items they desire

		[DataMember]
		public string Coupon; // the text they entered in the coupon box
	}

	/// <summary>
	/// This is returned to the client,
	/// it will show all the items in their cart along with
	/// prices and a total. Note if a coupon was used, and it was valid
	/// there will be extra line items which will reflect the coupons 
	/// discount ect.
	/// </summary>
	[DataContract]
	public class PurchaseOrder
	{
		[DataMember]
		public IEnumerable<Product> Items; //the stuff they are buying

		[DataMember]
		public decimal Total; //how much everything is going to cost

		[DataMember]
		public bool? Applied; // was the coupon used/vaild

		[DataMember]
		public string TermsNConditions; //the terms and conditions of sale (think this has been written? I doubt it)
	}

	[DataContract]
	public class BillingInfo
	{
		[DataMember]
		public CCTypes CardType;

		[DataMember]
		public string CCNumber;

		[DataMember]
		public string SecurityCode;

		[DataMember]
		public DateTime CCExpirationDate;

		[DataMember]
		public string FirstName;

		[DataMember]
		public string LastName;

		[DataMember]
		public string StreetAddress;

		[DataMember]
		public string SuiteApt;

		[DataMember]
		public string City;

		[DataMember]
		public string State;

		[DataMember]
		public string Zip;

        [DataMember]
        public bool StoreCC;
    
        [DataMember]
        public bool AutoRenew;
    }

	[DataContract]
	public enum CCTypes
	{
		[EnumMember]
		VISA = 1,
		[EnumMember]
		AmericanExpress = 2,
		[EnumMember]
		MasterCard = 3
	}
	[DataContract]
	public enum CartItemType
	{
		[EnumMember]
		SubscriptionPlan = 1,
		[EnumMember]
		PromotionalItem = 2,
		[EnumMember]
		ItemDiscount = 3
	}


	[DataContract]
	public class PurchaseReceipt : PurchaseOrder
	{
		[DataMember]
		public string confirmationNumber;

		public string CreditCardNumber;

        [DataMember]
        public BillingInfo BillingInfo;
	}
}
