﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;


namespace SkyLinkWCFService.DataTypes
{
    /// <summary>
    /// <remarks>added pin and secret question 5/27/11</remarks>
    /// </summary>
	[DataContract(Name = "SkyLinkUserInformation", Namespace = "http://www.mysky-link.com/skylink")]
	public class SkyLinkUserInformation
	{
		[DataMember]
		public int Id { get; set; }

		[DataMember]
		public string FirstName { get; set; }

		[DataMember]
		public string LastName { get; set; }

		[DataMember]
		public string StreetAddress1 { get; set; }

		[DataMember]
		public string StreetAddress2 { get; set; }

		[DataMember]
		public string Suite	{get;set;}

		[DataMember]
		public string City { get; set; }

		[DataMember]
		public string State { get; set; }

		[DataMember]
		public string PostalCode { get; set; }

		[DataMember]
		public string EmailAddress { get; set; }

		[DataMember]
		public string AlternateEmail { get; set; }

		[DataMember]
		public string PrimaryPhone { get; set; }

		[DataMember]
		public string CellPhone { get; set; }

		[DataMember]
		public string CellProvider	{ get; set; }

		[DataMember]
		public bool CanAddVehicles { get; set; }

		[DataMember]
		public bool CanRenewSubscription { get; set; }

		[DataMember]
		public bool CanManageUsers { get; set; }

    [DataMember]
    public int? PIN { get; set; }

		[DataMember]
		public VehicleManagement.Data.ConsumerNotifications EmailNotifications
		{
			get;
			set;
		}

		[DataMember]
		public VehicleManagement.Data.ConsumerNotifications SmsNotifications
		{
			get; set;
		}


		[DataMember]
		public string SecretQuestionAnswer
		{
			get;
			set;
		}

		[DataMember]
		public int SecretQuestionChoice
		{
			get;
			set;
		}

        [DataMember]
        public VehicleManagement.Data.TimeZone TimeZone
        {
            get;
            set;
        }

        [DataMember]
        public bool AutoRenew
        {
            get;
            set;
        }

	}
}