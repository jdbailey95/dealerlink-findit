﻿using System;
using System.Runtime.Serialization;
using VehicleManagement.Data;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract(Namespace = "http://mysky-link.com/skylink")]
    public class CreditCard
    {
        [DataMember]
        public CreditCardType CreditCardType
        {
            get;
            set;
        }

        [DataMember]
        public string Number
        {
            get;
            set;
        }

        [DataMember]
        public string Last4Digits
        {
            get;
            set;
        }

        [DataMember]
        public string Memo
        {
            get;
            set;
        }

        [DataMember]
        public DateTime ExpireDate
        {
            get;
            set;
        }

        [DataMember]
        public string FirstName
        {
            get;
            set;
        }

        [DataMember]
        public string LastName
        {
            get;
            set;
        }

        [DataMember]
        public string StreetAddress1
        {
            get;
            set;
        }

        [DataMember]
        public string Suite
        {
            get;
            set;
        }

        [DataMember]
        public string City
        {
            get;
            set;
        }

        [DataMember]
        public string State
        {
            get;
            set;
        }

        [DataMember]
        public string Zip
        {
            get;
            set;
        }
    }
}