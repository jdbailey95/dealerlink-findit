﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System.ServiceModel;


namespace SkyLinkWCFService.DataTypes
{
    [DataContract(Namespace = "http://mysky-link.com/skylink")]

    public class VehicleEventsResponse
    {
        public VehicleEventsResponse()
        {
            this.vehicles = new List<Vehicle>();
            this.eventList = new List<VehicleEvents>();
        }

        public void Add(Vehicle vehicle, VehicleEvents vehicleEvents)
        {
            this.vehicles.Add(vehicle);
            this.eventList.Add(vehicleEvents);
        }

        [DataMember]
        public List<Vehicle> vehicles
        {
            get;
            set;
        }
        
        [DataMember]
        public List<VehicleEvents> eventList
        {
            get;
            set;
        }
    }
}