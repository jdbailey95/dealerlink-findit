﻿using System;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    public class State
    {
        public State(int id, string code, string name)
        {
            this.ID = id;
            this.Code = code;
            this.Name = name;
        }

        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public string Name { get; set; }
    }
}
