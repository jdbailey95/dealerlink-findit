﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes.Faults
{
	[DataContract]
	public class ParameterFault
	{
		[DataMember]
		public ParameterFaultType FaultType;

		[DataMember]
		public string AdditionalInfo;
	}

	[DataContract]
	public enum ParameterFaultType
	{
		[EnumMember]
		InvalidType = 1,
		[EnumMember]
		InvalidRange = 2
	}
}
