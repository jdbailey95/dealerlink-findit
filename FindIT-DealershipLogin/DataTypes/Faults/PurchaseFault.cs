﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes.Faults
{
	[DataContract]
	public class PurchaseFault
	{
		[DataMember]
		public PurchaseFaultType FaultType
		{
			get;
			set;
		}

		[DataMember]
		public string AdditionalInfo
		{
			get;
			set;
		}
	}

	[DataContract]
	public enum PurchaseFaultType
	{
		[EnumMember]
		PaymentMethodDeclined = 1,
		[EnumMember]
		InvalidProduct = 2,
		[EnumMember]
		Other = 3
	}

}
