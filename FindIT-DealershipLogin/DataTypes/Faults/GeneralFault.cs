﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes.Faults
{
	[DataContract]
	public class GeneralFault 
	{
		[DataMember]
		public GeneralFaultType FaultType { get; set; }

		[DataMember]
		public string AdditionalInfo { get; set; }
	}

	[DataContract]
	public enum GeneralFaultType : byte
	{
		[EnumMember]
		DateRangeInvalid = 1,
		[EnumMember]
		UnknownUser = 2,
		[EnumMember]
		NotAuthorized = 3,
		[EnumMember]
		InvalidParameterCombination = 4,
		[EnumMember]
		InvalidParameter = 5,
		[EnumMember]
		InternalServerError = 6,
		[EnumMember]
		NotImplemented = 7,
		[EnumMember]
		ValidationFailure = 8,
		[EnumMember]
		GeneralFault = 9,
		[EnumMember]
		Other = 255

	}
}