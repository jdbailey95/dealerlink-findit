﻿using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes.Faults
{
	/// <summary>
	/// Fault in the database access layer
	/// </summary>
	[DataContract]
	public class DatabaseFault
	{
		/// <summary>
		/// Type of fault
		/// </summary>
		[DataMember]
		public DataBaseFaultType FaultType
		{
			get;
			set;
		}

		/// <summary>
		/// Additional information about the exception
		/// </summary>
		[DataMember]
		public string AdditionalInfo
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Enumeration of possible database faults
	/// </summary>
	[DataContract]
	public enum DataBaseFaultType
	{
		/// <summary>
		/// The requested record was not found
		/// </summary>
		[EnumMember]
		RecordNotFound = 1,

		/// <summary>
		/// An email address was duplicated
		/// </summary>
		[EnumMember]
		EmailAddressDuplication = 2,

		/// <summary>
		/// The user is unknown
		/// </summary>
		[EnumMember]
		UnknownUser = 3
	}
}