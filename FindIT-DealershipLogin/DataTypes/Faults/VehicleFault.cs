﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SkyLinkWCFService.DataTypes.Faults
{
	[DataContract]
	public class VehicleFault
	{
		[DataMember]
		public VehicleFaultType FaultType { get; set; }

		[DataMember]
		public string AdditionalInformation { get; set; }
	}

	[DataContract]
	public enum VehicleFaultType
	{
		[EnumMember]
		UnknownVin = 1,
		[EnumMember]
		VehicleNotOwnedByUser = 2,
		[EnumMember]
		NoDevice = 3,
		[EnumMember]
		UserNotAuthorized = 4
	}
}