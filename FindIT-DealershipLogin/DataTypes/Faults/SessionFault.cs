﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes.Faults
{

	[DataContract]
	public class SessionFault
	{
		[DataMember]
		public SessionFaultType FaultType { get; set; }

		[DataMember]
		public string AdditionalInfo { get; set; }
	}

	[DataContract]
	public enum SessionFaultType
	{
		[EnumMember]
		SessionExpired = 1,

		[EnumMember]
		NotLoggedIn = 2,

		[EnumMember]
		UnknownUser = 3
	}

}
