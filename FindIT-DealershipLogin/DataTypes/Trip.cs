﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace SkyLinkWCFService.DataTypes
{
	[DataContract(Name = "Trip", Namespace = "http://mysky-link.com/skylink")]
	public class Trip
	{
		public Trip()
		{

		}

		public Trip(LocationPoint tripStart, LocationPoint tripEnd)
		{
			this.TripStart = tripStart;
			this.TripEnd = tripEnd;
			this.MaxSpeed = tripStart.Speed > tripEnd.Speed ? tripStart.Speed : tripEnd.Speed;
		}

		public Trip(LocationPoint tripStart, LocationPoint tripEnd, int distance)
			: this(tripStart, tripEnd)
		{
			this.Duration = tripEnd.TimeStamp - tripStart.TimeStamp;
			this.Distance = distance;
		}

		/// <summary>
		/// Starting location
		/// </summary>
		[DataMember]
		public LocationPoint TripStart
		{
			get;
			set;
		}

		/// <summary>
		/// Ending Location
		/// </summary>
		[DataMember]
		public LocationPoint TripEnd
		{
			get;
			set;
		}

		/// <summary>
		/// Maximum speed attained during this trip
		/// </summary>
		[DataMember]
		public double? MaxSpeed
		{
			get;
			set;
		}

		/// <summary>
		/// Duration of the trip in seconds.
		/// </summary>
		[DataMember]
		public TimeSpan Duration
		{
			get;
			set;
		}

		/// <summary>
		/// Distance between start and stop locations
		/// </summary>
		[DataMember]
		public int? Distance
		{
			get;
			set;
		}
	}

	[DataContract(Name = "TripSummary", Namespace = "http://mysky-link.com/skylink")]
	public class TripSummary : Trip
	{
		public TripSummary()
		{

		}
		public TripSummary(LocationPoint tripStart, LocationPoint tripEnd)
			: base(tripStart, tripEnd)
		{

		}
	}

	[DataContract(Name = "TripInfo", Namespace = "http://mysky-link.com/skylink")]
	public class TripInfo : Trip
	{
		/// <summary>
		/// Locations after trip start and trip end. The oldest location (closest to start) will
		/// be first. Start and Stop locations will not be in the collection
		/// </summary>
		[DataMember]
		public IEnumerable<LocationPoint> TripLocations
		{
			get;
			set;
		}
	}

	[DataContract]
	public enum TripRequestType
	{
		[EnumMember]
		Summary,
		[EnumMember]
		Detailed
	}
}