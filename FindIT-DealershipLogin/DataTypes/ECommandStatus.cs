﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    public enum ECommandStatus
    {
        [EnumMember]
        Success = 0,

        [EnumMember]
        Failed = 1,

        [EnumMember]
        InProgress = 2,

        [EnumMember]
        DeviceNotAccessible = 3,

				[EnumMember]
				Cancelled = 4
    }
}