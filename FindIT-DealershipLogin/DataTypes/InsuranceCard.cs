﻿using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	/// <summary>
	/// Contains information about insurance
	/// </summary>
	[DataContract]
	public class InsuranceCard
	{
		/// <summary>
		/// The vehicle the insurance pertains to
		/// </summary>
		[DataMember]
		public Vehicle Vehicle
		{
			get;
			set;
		}

		/// <summary>
		/// Information about the owner
		/// </summary>
		[DataMember]
		public SkyLinkUserInformation OwnerInformation
		{
			get;
			set;
		}

		/// <summary>
		/// The dealership
		/// </summary>
		[DataMember]
		public DealerShip Dealership
		{
			get;
			set;
		}

		/// <summary>
		/// Carrier for the insurance
		/// </summary>
		[DataMember]
		public string InsuranceCarrier
		{
			get;
			set;
		}

		/// <summary>
		/// Name of the insurance agent
		/// </summary>
		[DataMember]
		public string AgentName
		{
			get;
			set;
		}

		/// <summary>
		/// Policy number of the insurance policy
		/// </summary>
		[DataMember]
		public string PolicyNumber
		{
			get;
			set;
		}

		/// <summary>
		/// Phone number for the insurance agent
		/// </summary>
		[DataMember]
		public string AgentPhoneNumber
		{
			get;
			set;
		}
	}
}