﻿using System;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	/// <summary>
	/// not sure if I want to use this
	/// </summary>
	[Obsolete("not sure if we want to use an enum here.")]
	[DataContract]
	public enum EVehicleMake
	{
		/// <summary>
		/// Acura
		/// </summary>
		[EnumMember]
		Acura = 1,

		/// <summary>
		/// Alfa Romeo
		/// </summary>
		[EnumMember]
		Alfa_Romeo,

		/// <summary>
		/// AMC
		/// </summary>
		[EnumMember]
		AMC,

		/// <summary>
		/// Aston Martin
		/// </summary>
		[EnumMember]
		Aston_Martin,

		/// <summary>
		/// Audi
		/// </summary>
		[EnumMember]
		Audi,

		/// <summary>
		/// Avanti
		/// </summary>
		[EnumMember]
		Avanti,

		/// <summary>
		/// Bentley
		/// </summary>
		[EnumMember]
		Bentley,

		/// <summary>
		/// BMW
		/// </summary>
		[EnumMember]
		BMW,

		/// <summary>
		/// Buick
		/// </summary>
		[EnumMember]
		Buick,

		/// <summary>
		/// Cadillac
		/// </summary>
		[EnumMember]
		Cadillac,

		/// <summary>
		/// Chevrolet
		/// </summary>
		[EnumMember]
		Chevrolet,

		/// <summary>
		/// Chrysler
		/// </summary>
		[EnumMember]
		Chrysler,

		/// <summary>
		/// Daewoo
		/// </summary>
		[EnumMember]
		Daewoo,

		/// <summary>
		/// Daihatsu
		/// </summary>
		[EnumMember]
		Daihatsu,

		/// <summary>
		/// Datsun
		/// </summary>
		[EnumMember]
		Datsun,

		/// <summary>
		/// DeLorean
		/// </summary>
		[EnumMember]
		DeLorean,

		/// <summary>
		/// Dodge
		/// </summary>
		[EnumMember]
		Dodge,

		/// <summary>
		/// Eagle
		/// </summary>
		[EnumMember]
		Eagle,

		/// <summary>
		/// Ferrari
		/// </summary>
		[EnumMember]
		Ferrari,

		/// <summary>
		/// Fiat
		/// </summary>
		[EnumMember]
		Fiat,

		/// <summary>
		/// Ford
		/// </summary>
		[EnumMember]
		Ford,

		/// <summary>
		/// Geo
		/// </summary>
		[EnumMember]
		Geo,

		/// <summary>
		/// GMC
		/// </summary>
		[EnumMember]
		GMC,

		/// <summary>
		/// Honda
		/// </summary>
		[EnumMember]
		Honda,

		/// <summary>
		/// Hummer
		/// </summary>
		[EnumMember]
		HUMMER,

		/// <summary>
		/// Hyundai
		/// </summary>
		[EnumMember]
		Hyundai,

		/// <summary>
		/// Infiniti
		/// </summary>
		[EnumMember]
		Infiniti,

		/// <summary>
		/// Isuzu
		/// </summary>
		[EnumMember]
		Isuzu,

		/// <summary>
		/// Jaguar
		/// </summary>
		[EnumMember]
		Jaguar,

		/// <summary>
		/// Jeep
		/// </summary>
		[EnumMember]
		Jeep,

		/// <summary>
		/// Kia
		/// </summary>
		[EnumMember]
		Kia,

		/// <summary>
		/// Lamborghini
		/// </summary>
		[EnumMember]
		Lamborghini,

		/// <summary>
		/// Lancia
		/// </summary>
		[EnumMember]
		Lancia,

		/// <summary>
		/// Land Rover
		/// </summary>
		[EnumMember]
		Land_Rover,

		/// <summary>
		/// Lexus
		/// </summary>
		[EnumMember]
		Lexus,

		/// <summary>
		/// Lincoln
		/// </summary>
		[EnumMember]
		Lincoln,

		/// <summary>
		/// Lotus
		/// </summary>
		[EnumMember]
		Lotus,

		/// <summary>
		/// Mazerati
		/// </summary>
		[EnumMember]
		Maserati,

		/// <summary>
		/// Maybach
		/// </summary>
		[EnumMember]
		Maybach,

		/// <summary>
		/// Mazda
		/// </summary>
		[EnumMember]
		Mazda,

		/// <summary>
		/// Mercedes Benz
		/// </summary>
		[EnumMember]
		Mercedes_Benz,

		/// <summary>
		/// Mercury
		/// </summary>
		[EnumMember]
		Mercury,

		/// <summary>
		/// Merkur
		/// </summary>
		[EnumMember]
		Merkur,

		/// <summary>
		/// Mini
		/// </summary>
		[EnumMember]
		MINI,

		/// <summary>
		/// Mitsubishi
		/// </summary>
		[EnumMember]
		Mitsubishi,

		/// <summary>
		/// Nissan
		/// </summary>
		[EnumMember]
		Nissan,

		/// <summary>
		/// Oldsmobile
		/// </summary>
		[EnumMember]
		Oldsmobile,

		/// <summary>
		/// Peugeot
		/// </summary>
		[EnumMember]
		Peugeot,

		/// <summary>
		/// Plymouth
		/// </summary>
		[EnumMember]
		Plymouth,

		/// <summary>
		/// Pontiac
		/// </summary>
		[EnumMember]
		Pontiac,

		/// <summary>
		/// Porsche
		/// </summary>
		[EnumMember]
		Porsche,

		/// <summary>
		/// Renault
		/// </summary>
		[EnumMember]
		Renault,

		/// <summary>
		/// Rolls Royce
		/// </summary>
		[EnumMember]
		Rolls_Royce,

		/// <summary>
		/// Saab
		/// </summary>
		[EnumMember]
		Saab,

		/// <summary>
		/// Saturn
		/// </summary>
		[EnumMember]
		Saturn,

		/// <summary>
		/// Scion
		/// </summary>
		[EnumMember]
		Scion,

		/// <summary>
		/// Smart
		/// </summary>
		[EnumMember]
		smart,

		/// <summary>
		/// Sterling
		/// </summary>
		[EnumMember]
		Sterling,

		/// <summary>
		/// Subaru
		/// </summary>
		[EnumMember]
		Subaru,

		/// <summary>
		/// Suzuki
		/// </summary>
		[EnumMember]
		Suzuki,

		/// <summary>
		/// Toyota
		/// </summary>
		[EnumMember]
		Toyota,

		/// <summary>
		/// Triumph
		/// </summary>
		[EnumMember]
		Triumph,

		/// <summary>
		/// Volkswagen
		/// </summary>
		[EnumMember]
		Volkswagen,

		/// <summary>
		/// Volvo
		/// </summary>
		[EnumMember]
		Volvo,

		/// <summary>
		/// Yugo
		/// </summary>
		[EnumMember]
		Yugo,

		/// <summary>
		/// Tesla
		/// </summary>
		[EnumMember]
		Tesla
	}
}