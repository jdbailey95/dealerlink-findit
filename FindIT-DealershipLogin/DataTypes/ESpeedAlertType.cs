﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
    [DataContract]
    public enum ESpeedAlertType
    {
        [EnumMember]
        Immediately = 0,

        [EnumMember]
        EndOfTrip = 1
    }
}