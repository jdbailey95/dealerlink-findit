﻿using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	[DataContract(Namespace = "http://mysky-link.com/skylink")]
	public class Vehicle
	{
		[DataMember]
		public string Vin
		{
			get;
			set;
		}

		[DataMember]
		public string Name
		{
			get;
			set;
		}

		[DataMember]
		public string Make
		{
			get;
			set;
		}

		[DataMember]
		public string Model
		{
			get;
			set;
		}

		[DataMember]
		public short Year
		{
			get;
			set;
		}

		[DataMember]
		public string Color
		{
			get;
			set;
		}

		[DataMember]
		public bool isSpeedAlertSet
		{
			get;
			set;
		}

		[DataMember]
		public LinkSubscriptionPlan[] SkyLinkPlan
		{
			get;
			set;
		}

		[DataMember]
		public bool isQuickFenceEnabled
		{
			get;
			set;
		}

		[DataMember]
		public bool isQuickFenceViolated
		{
			get;
			set;
		}

		[DataMember]
		public bool isBatteryAlertEnabled
		{
			get;
			set;
		}

		[DataMember]
		public float? BatteryAlarmLevel
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the device serial.
		/// </summary>
		/// <value>The device serial.</value>
		/// <remarks>added 2 aug 11, for use in Insurance card</remarks>
		[DataMember]
		public string DeviceSerial
		{
			get; set; }
    
        [DataMember]
        public string LicensePlate
        {
            get;
            set;
        }

        [DataMember]
        public LocationPoint LastLocation
        {
            get;
            set;
        }

    }
}