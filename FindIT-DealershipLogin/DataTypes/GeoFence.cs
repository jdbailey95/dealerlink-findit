﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	/// <summary>
	/// Geofence geometry types
	/// </summary>
	[DataContract]
	public enum GeofenceGeometryType
	{
		/// <summary>
		/// A circular geofence
		/// </summary>
		[EnumMember]
		Circular,

		/// <summary>
		/// A rectangular geofence
		/// </summary>
		[EnumMember]
		Rectangular,

		/// <summary>
		/// A polygon geofence
		/// </summary>
		[EnumMember]
		Polygon
	}

	/// <summary>
	/// Base class for geofences
	/// </summary>
	[DataContract]
	[KnownType(typeof(CircularGeofence))]
	[KnownType(typeof(RectangleGeofence))]
	[KnownType(typeof(PolygonGeofence))]
	abstract public class Geofence
	{
		/// <summary>
		/// Index of the fence
		/// </summary>
		[DataMember]
		public int Index
		{
			get;
			set;
		}

		/// <summary>
		/// Name of the geofence
		/// </summary>
		[DataMember]
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Type of fence trigger
		/// </summary>
		[DataMember]
		public EFenceTriggerTypes TriggerType
		{
			get;
			set;
		}

		/// <summary>
		/// Whether or not the fence is enabled
		/// </summary>
		[DataMember]
		public bool IsEnabled
		{
			get;
			set;
		}

		/// <summary>
		/// Geometry type of the fence
		/// </summary>
		[DataMember]
		public GeofenceGeometryType Geometry
		{
			get;
			set;
		}

		/// <summary>
		/// Description of the fence
		/// </summary>
		[DataMember]
		public string Description
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Contains info about circular geofences
	/// </summary>
	[DataContract]
	public class CircularGeofence : Geofence
	{
		/// <summary>
		/// Constructor for circular geofences
		/// </summary>
		public CircularGeofence()
		{
			this.Geometry = GeofenceGeometryType.Circular;
		}

		/// <summary>
		/// Center of the fence
		/// </summary>
		[DataMember]
		public GeoPoint Center
		{
			get;
			set;
		}

		/// <summary>
		/// Radius of the fence
		/// </summary>
		[DataMember]
		public int Radius
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Contains info about rectangular geofences
	/// </summary>
	[DataContract]
	public class RectangleGeofence : Geofence
	{
		/// <summary>
		/// Constructor for rectangular geofences
		/// </summary>
		public RectangleGeofence()
		{
			this.Geometry = GeofenceGeometryType.Rectangular;
		}

		/// <summary>
		/// The north-westernmost point
		/// </summary>
		[DataMember]
		public GeoPoint NorthWestPoint
		{
			get;
			set;
		}

		/// <summary>
		/// The south-easternmost point
		/// </summary>
		[DataMember]
		public GeoPoint SouthEastPoint
		{
			get;
			set;
		}
	}

	/// <summary>
	/// Contains info about polygon fences
	/// </summary>
	[DataContract]
	public class PolygonGeofence : Geofence
	{
		/// <summary>
		/// Constructor for polygon geofences
		/// </summary>
		public PolygonGeofence()
		{
			this.Geometry = GeofenceGeometryType.Polygon;
		}

		/// <summary>
		/// Points which comprise the fence
		/// </summary>
		[DataMember]
		public IEnumerable<GeoPoint> Points;
	}
}