﻿using System;
using System.Runtime.Serialization;


namespace SkyLinkWCFService.DataTypes
{
    [DataContract(Namespace = "http://mysky-link.com/skylink")]
    public class Area : GeoPoint
    {
        public Area()
        {
            
        }

        public Area(double lat, double lon) : base(lat,lon)
        {
            
        }

        public Area(GeoPoint geoPoint):base(geoPoint.Lat,geoPoint.Lon)
        {
            
        }

        public Area(int id,double lat,
            double lon,
            int radius
            ):this(lat,lon)
        {
            this.Id = id;
            this.Radius = radius;
        }

        [DataMember]
        public int Id { get; set; }
        
        [DataMember]
        public int Radius{ get; set; }

    }
}