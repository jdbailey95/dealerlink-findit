﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using VehicleManagement.Data;


namespace SkyLinkWCFService.DataTypes
{
    [DataContract(Namespace = "http://mysky-link.com/skylink")]
    public class LocationPoint : GeoPoint
    {
        public LocationPoint()
        {
            
        }

        public LocationPoint(double lat, double lon) : base(lat,lon)
        {
            
        }

        public LocationPoint(GeoPoint geoPoint):base(geoPoint.Lat,geoPoint.Lon)
        {
            
        }

        public LocationPoint(int id,double lat,
            double lon,
            DateTime timeStamp,
            string streetAddress,
            string city,
            string state,
            string zip,
            string county,
            string country,
            bool ignitionOn,
            short heading,
            double speed,
            LocationAlarmType? locationAlarmType,
            int? locationAlarmIndex
            ):this(lat,lon)
        {
            this.Id = id;
            this.StreetAddress = streetAddress;
            this.City = city;
            this.State = state;
            this.Zip = zip;
            this.County = county;
            this.Country = country;
            this.IgnitionOn = ignitionOn;
            this.Heading = heading;
            this.Speed = speed;
            this.LocationAlarmType = locationAlarmType;
            this.LocationAlarmIndex = locationAlarmIndex;
        }

        [DataMember]
        public int Id { get; set; }
        
        [DataMember]
        public DateTime TimeStamp { get; set; }

        [DataMember]
        public string StreetAddress { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Zip { get; set; }

        [DataMember]
        public string County { get; set; }

        [DataMember]
        public string Country { get; set; }

			/// <summary>
			/// 4/19 changed to nullable
			/// </summary>
        [DataMember]
        public bool? IgnitionOn { get; set; }

			/// <summary>
			/// 4/19 changed to nullable as the heading might not have been reported
			/// </summary>
        [DataMember]
        public short? Heading { get; set; }

				/// <summary>
				/// 4/19 changed to nullable
				/// </summary>
        [DataMember]
        public double? Speed { get; set; }

        [DataMember]
        public LocationAlarmType? LocationAlarmType { get; set; }

        [DataMember]
        public int? LocationAlarmIndex { get; set; }

        [DataMember]
        public bool? AlarmCleared { get; set; }

    }
}