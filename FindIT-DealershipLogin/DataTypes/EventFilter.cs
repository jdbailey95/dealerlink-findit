﻿using System;
using System.Runtime.Serialization;


namespace SkyLinkWCFService.DataTypes
{

    [DataContract(Namespace = "http://mysky-link.com/skylink")]
    public class EventFilter
    {
        public EventFilter()
        {
            
        }

        [DataMember]
        public string[] locationAlertTypes { get; set; }

        [DataMember]
        public int maxCount { get; set; }

        [DataMember]
        public DateTime startDate { get; set; }

        [DataMember]
        public DateTime endDate { get; set; }

        [DataMember]
        public Area area{ get; set; }

        [DataMember]
        public string ackType { get; set; }
    }
}