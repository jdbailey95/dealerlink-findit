﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	/// <summary>
	/// Vehicle maintenance interval status types
	/// </summary>
	[DataContract]
	public enum VehicleMaintenanceIntervalStatus
	{
        /// <summary>
        /// Maintenance is current
        /// </summary>
        [EnumMember]
        Current,
        
        /// <summary>
		/// Maintenance is upcoming (i.e. at or past threshold1)
		/// </summary>
		[EnumMember]
		Upcoming,

		/// <summary>
        /// Maintenance is due (i.e. at or past threshold2)
        /// </summary>
		[EnumMember]
		Due,

		/// <summary>
        /// Maintenance is past due (i.e. at or past threshold3)
        /// </summary>
		[EnumMember]
		Overdue
	}

    /// <summary>
    /// MaintenanceMetrics
    /// </summary>
    public class VehicleMaintenanceMetrics
    {
        /// <summary>
        /// Date
        /// </summary>
        public System.DateTime? Date
        {
            get;
            set;
        }

        /// <summary>
        /// Odometer reading
        /// </summary>
        [DataMember]
        public int? Odometer
        {
            get;
            set;
        }

        /// <summary>
        /// Number of seconds of use
        /// </summary>
        [DataMember]
        public int? Utilization
        {
            get;
            set;
        }
    }

    /// <summary>
    /// VehicleMaintenanceInterval
    /// </summary>
	[DataContract]
	public class VehicleMaintenanceInterval
	{
        /// <summary>
		/// Index of the interval
		/// </summary>
		[DataMember]
		public int Index
		{
			get;
			set;
		}

        /// <summary>
        /// Related Maintenance ID
        /// </summary>
        [DataMember]
        public int MaintenanceIntervalID
        {
            get;
            set;
        }

        /// <summary>
		/// Name of the interval
		/// </summary>
		[DataMember]
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Description of the fence
		/// </summary>
		[DataMember]
		public string Description
		{
			get;
			set;
		}

        /// <summary>
        /// Status of the interval
        /// </summary>
        [DataMember]
        public VehicleMaintenanceIntervalStatus Status
        {
            get;
            set;
        }

        /// <summary>
        /// Metrics when this maintenance interval started
        /// </summary>
        [DataMember]
        public VehicleMaintenanceMetrics Start
        {
            get;
            set;
        }

        /// <summary>
        /// Who started this maintenance interval
        /// </summary>
        [DataMember]
        public int StartUser
        {
            get;
            set;
        }

        /// <summary>
        /// Metrics when this maintenance interval will be due
        /// </summary>
        [DataMember]
        public VehicleMaintenanceMetrics Due
        {
            get;
            set;
        }

        /// <summary>
        /// Most recently recorded metrics
        /// </summary>
        [DataMember]
        public VehicleMaintenanceMetrics Recorded
        {
            get;
            set;
        }

        /// <summary>
        /// Percentage that recorded metrics represent towards maintenance being due
        /// </summary>
        [DataMember]
        public MaintenanceMetricPercentages DuePercent
        {
            get;
            set;
        }

    }
}