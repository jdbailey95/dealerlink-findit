﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	[DataContract]
	public enum EFenceGeometery
	{
		[EnumMember]
		Circular = 0,

		[EnumMember]
		Polygon = 1,

		[EnumMember]
		Rectangle = 2,

		[EnumMember]
		Quick = 3

	}
}