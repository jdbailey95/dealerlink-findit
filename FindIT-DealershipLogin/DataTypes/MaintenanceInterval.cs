﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.DataTypes
{
	/// <summary>
	/// Vehicle maintenance interval status types
	/// </summary>
	[DataContract]
	public enum MaintenanceIntervalStatus
	{
        /// <summary>
        /// Maintenance is current
        /// </summary>
        [EnumMember]
        Active,
        
        /// <summary>
		/// Maintenance is upcoming (i.e. at or past threshold1)
		/// </summary>
		[EnumMember]
		Inactive,

		/// <summary>
        /// Maintenance is due (i.e. at or past threshold2)
        /// </summary>
		[EnumMember]
		Deleted
	}

    /// <summary>
    /// MaintenanceMetrics
    /// </summary>
    public class MaintenanceMetrics
    {
        /// <summary>
        /// Date
        /// </summary>
        public int? Days
        {
            get;
            set;
        }

        /// <summary>
        /// Distance
        /// </summary>
        [DataMember]
        public int? Distance
        {
            get;
            set;
        }

        /// <summary>
        /// Number of hours of use
        /// </summary>
        [DataMember]
        public int? Hours
        {
            get;
            set;
        }
    }

    /// <summary>
    /// MaintenanceMetricPercentages
    /// </summary>
    public class MaintenanceMetricPercentages
    {
        /// <summary>
        /// % days towards being due
        /// </summary>
        public int? Days
        {
            get;
            set;
        }

        /// <summary>
        /// % Distance towards being due
        /// </summary>
        [DataMember]
        public int? Distance
        {
            get;
            set;
        }

        /// <summary>
        /// % utilization towards being due
        /// </summary>
        [DataMember]
        public int? Utilization
        {
            get;
            set;
        }
    }

    /// <summary>
    /// MaintenanceIntervalMetrics
    /// </summary>
    public class MaintenanceThreshold
    {
        /// <summary>
        /// Days
        /// </summary>
        public int? Days
        {
            get;
            set;
        }

        /// <summary>
        /// Distance
        /// </summary>
        [DataMember]
        public int? Distance
        {
            get;
            set;
        }

        /// <summary>
        /// Number of hours of use
        /// </summary>
        [DataMember]
        public int? Hours
        {
            get;
            set;
        }
    }

    /// <summary>
    /// MaintenanceInterval
    /// </summary>
	[DataContract]
	public class MaintenanceInterval
	{
        /// <summary>
        /// Maintenance interval ID
        /// </summary>
        [DataMember]
        public int ID
        {
            get;
            set;
        }

        /// <summary>
        /// Account the interval is associated with
        /// </summary>
        [DataMember]
        public int AccountID
        {
            get;
            set;
        }
        
        /// <summary>
		/// Index of the interval
		/// </summary>
		[DataMember]
		public int Index
		{
			get;
			set;
		}

		/// <summary>
		/// Name of the interval
		/// </summary>
		[DataMember]
		public string Name
		{
			get;
			set;
		}

		/// <summary>
		/// Description of the fence
		/// </summary>
		[DataMember]
		public string Description
		{
			get;
			set;
		}

        /// <summary>
        /// Status of the interval
        /// </summary>
        [DataMember]
        public int Status
        {
            get;
            set;
        }

        /// <summary>
        /// Metrics when this maintenance interval started
        /// </summary>
        [DataMember]
        public MaintenanceMetrics Interval
        {
            get;
            set;
        }

        /// <summary>
        /// First threshold
        /// </summary>
        [DataMember]
        public MaintenanceThreshold Threshold1
        {
            get;
            set;
        }

        /// <summary>
        /// Second threshold
        /// </summary>
        [DataMember]
        public MaintenanceThreshold Threshold2
        {
            get;
            set;
        }

        /// <summary>
        /// Third threshold
        /// </summary>
        [DataMember]
        public MaintenanceThreshold Threshold3
        {
            get;
            set;
        }
    }
}