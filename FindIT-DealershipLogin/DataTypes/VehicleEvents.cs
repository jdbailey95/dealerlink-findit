﻿using System.Runtime.Serialization;
using System.Collections.Generic;
using System.ServiceModel;


namespace SkyLinkWCFService.DataTypes
{
    [DataContract(Namespace = "http://mysky-link.com/skylink")]

    public class VehicleEvents
    {
        public VehicleEvents(string vin)
        {
            this.Vin = vin;
        }

        public VehicleEvents(string vin, LocationPoint[] events)
        {
            this.Vin = vin;
            this.Events = events;
        }


        [DataMember]
        public string Vin
        {
            get;
            set;
        }

        [DataMember]
        public LocationPoint[] Events
        {
            get;
            set;
        }
    }
}