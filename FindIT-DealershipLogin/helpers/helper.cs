﻿namespace SkyLinkWCFService
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.ServiceModel;
	using GenericGeocoder;
	using ITP.Clients;
	using ITP.Common.Types;
	using SkyLinkWCFService.DataTypes;
	using SkyLinkWCFService.DataTypes.Faults;
	using VehicleManagement.Data;

	public static class Helper
	{
		#region Methods

		public static void CheckForDevice(this VehicleManagement.Data.Vehicle vehicle)
		{
			if (vehicle.Device == null)
			{
				throw new FaultException<VehicleFault>(new VehicleFault
				{
					FaultType = VehicleFaultType.NoDevice,
					AdditionalInformation = "No Device Installed"
				});
			}
		}

		public static VehicleManagement.Data.LocationAlertType ToDBType(this DataTypes.LocationAlarmType type)
		{
			switch (type)
			{
				case LocationAlarmType.None:
					return LocationAlertType.None;
				case LocationAlarmType.IgnitionOn:
					return LocationAlertType.IgnitionOn;
				case LocationAlarmType.IgnitionOff:
					return LocationAlertType.IgnitionOff;
				case LocationAlarmType.GeofenceEnter:
					return LocationAlertType.GeofenceEnter;
				case LocationAlarmType.GeofenceExit:
					return LocationAlertType.GeofenceExit;
				case LocationAlarmType.EarlyTheftDetection:
					return LocationAlertType.EarlyTheftDetection;
				case LocationAlarmType.OverSpeed:
					return LocationAlertType.OverSpeed;
				case LocationAlarmType.LowBatteryLevel:
					return LocationAlertType.LowBatteryLevel;
				case LocationAlarmType.PowerDisconnect:
					return LocationAlertType.PowerDisconnect;
				default:
					throw new FaultException<GeneralFault>(new GeneralFault
					{
						AdditionalInfo = "unknow LocationAlarmType",
						FaultType = GeneralFaultType.InvalidParameter
					});
			}
		}

		public static DeviceClient CreateDeviceClient(this AccountingClient accounting, DeviceIdentification device)
		{
			return new DeviceClient(accounting.GetDeviceServiceID(device));
		}

		public static ITP.Common.Types.GeoFence CreateDeviceCommand(this EnableFence fence)
		{
			ITP.Common.Types.GeoFence retval = null;
			switch (fence.CommandType)
			{
				case DeviceCommandType.EnableCircularGeofence:
					var cfence = (EnableCircularFence)fence;
					retval = new ITP.Common.Types.CircularGeofence
					{
						CenterPoint = new GPSPoint
						{
							Latitude = cfence.CenterLatitude,
							Longitude = cfence.CenterLongitude
						},
						Enabled = true,
						Index = cfence.FenceIndex,
						Name = cfence.Name,
						Radius = cfence.RadiusMeters,
						Trigger = cfence.TriggerType.ToITP()
					};
					break;
				case DeviceCommandType.EnableRectangularGeofence:
					var rfence = (EnableRectangularFence)fence;
					retval = new PolygonGeoFence
					{
						Enabled = true,
						Index = fence.FenceIndex,
						Name = fence.Name,
						Points = rfence.GetITPPoints(),
						Trigger = rfence.TriggerType.ToITP()
					};
					break;
				case DeviceCommandType.EnablePolyGeofence:
					throw new FaultException<DeviceFault>(new DeviceFault
					{
						FaultType = DeviceFaultType.UnsupportedFenceType,
						AdditionalInfo = "Device does not support polygon fences"
					});
				default:
					throw new FaultException<GeneralFault>(new GeneralFault
					{
						FaultType = GeneralFaultType.InvalidParameter,
						AdditionalInfo = "Unknown fence type"
					});
			}
			return retval;
		}

		/// <summary>
		/// Fills the consumer set attributes.
		/// </summary>
		/// <param name="vehicle">The WCF vehicle.</param>
		/// <param name="dbVehicle">The db vehicle.</param>
		/// <returns>the vehicle with the consumer settable attributes populated, when not present the vehicle values are used.</returns>
		public static SkyLinkWCFService.DataTypes.Vehicle FillConsumerSetAttributes(this SkyLinkWCFService.DataTypes.Vehicle vehicle, VehicleManagement.Data.Vehicle dbVehicle)
		{
			vehicle.Color = dbVehicle.OwnerColor ?? dbVehicle.Color;
			vehicle.Make = dbVehicle.OwnerMake ?? dbVehicle.Make;
			vehicle.Model = dbVehicle.OwnerModel ?? dbVehicle.Make;
			vehicle.Name = dbVehicle.OwnerName;
			vehicle.Vin = dbVehicle.VIN;
			vehicle.Year = dbVehicle.OwnerYear ?? dbVehicle.Year;

			return vehicle;
		}

		public static List<GPSPoint> GetITPPoints(this EnableRectangularFence rfence)
		{
			return new List<GPSPoint> {
                new GPSPoint {
                    Latitude = rfence.NorthLatitude,
                    Longitude = rfence.WestLongitude
                },
                new GPSPoint {
                    Longitude = rfence.EastLongitude,
                    Latitude = rfence.NorthLatitude
                },
                new GPSPoint {
                    Latitude = rfence.SouthLatitude,
                    Longitude = rfence.EastLongitude
                },
                new GPSPoint {
                    Longitude = rfence.WestLongitude,
                    Latitude = rfence.SouthLatitude
                }
            };
		}

		public static SkyLinkWCFService.DataTypes.LinkSubscriptionPlan[] GetWCFConsumersSubscriptionPlan(this VehicleManagement.Data.Vehicle vehicle)
		{
			var a = vehicle.LinkSubscriptionMaps.Where(x => x.Account == vehicle.Owner);

			if (a == null)
			{
				return null; // no subscription
			}

			List<SkyLinkWCFService.DataTypes.LinkSubscriptionPlan> lst = new List<SkyLinkWCFService.DataTypes.LinkSubscriptionPlan>();

			foreach (var plan in a)
			{
				lst.Add(new SkyLinkWCFService.DataTypes.LinkSubscriptionPlan
				{
					Description = plan.LinkSubscriptionPlan.Description,
					ExpirationDate = plan.PlanExpiration,
					Name = plan.LinkSubscriptionPlan.Name
				});
			}

			return lst.ToArray();
		}

		public static bool IsNullOrEmpty(this string str)
		{
			if (str == null || str == string.Empty)
				return true;

			return false;
		}

		public static IEnumerable<VehicleManagement.Data.LinkSubscriptionPlan> GetConsumerSubscriptionPlan(this VehicleManagement.Data.Vehicle veh)
		{
			return veh.LinkSubscriptionMaps.Where(x => x.Account.GetType() == typeof(ConsumerAccount))
											.Select(p => p.LinkSubscriptionPlan);
		}

		/// <summary>
		/// Sends the command information to the DTS for ANY command
		/// </summary>
		/// <param name="command">The command to send</param>
		/// <exception cref="SkyLinkWCFService.DataTypes.GeneralFault">Thrown when an unknown command type is encountered.  Should never happen.</exception>
		/// <remarks>When we clean up the instantiation of a DTS instance to pull info from the accounting client, this is the ONLY place this need occur!!!!!</remarks>
		public static void SendToDTS(this DeviceCommand command)
		{
			try
			{
				using (AccountingClient accounting = new AccountingClient())
				{
					DeviceIdentification deviceID = new DeviceIdentification
					{
						DeviceID = command.Device.DeviceID,
						ServiceID = command.Device.NetworkID
					};
					using (DeviceClient dts = accounting.CreateDeviceClient(deviceID))
					{
						switch (command.CommandType)
						{
							case DeviceCommandType.Locate:
								dts.RequestLocation(deviceID, command.CommandID, null);
								break;
							case DeviceCommandType.SetDigitalOutput:
								//TODO: make this work
								break;
							case DeviceCommandType.UpgradeScript:
								dts.UpgradeConfiguration(deviceID, command.CommandID, null, ((UpgradeScript)command).ScriptVersion.Version);
								break;
							case DeviceCommandType.UpgradeFirmware:
								dts.UpgradeFirmware(deviceID, command.CommandID, null, ((UpgradeFirmware)command).FirmwareVersion.Version);
								break;
							case DeviceCommandType.SetSpeedThreshold:
								dts.SetSpeedAlert(deviceID, ((SetOverspeed)command).Speed, command.CommandID, null);
								break;
							case DeviceCommandType.SetBatteryLevel:
								dts.SetBatteryAlarmThreshold(deviceID, command.CommandID, null, ((SetBatteryAlarmLevel)command).Threshold);
								break;
							case DeviceCommandType.EnableBatteryAlarm:
								float oldValue = command.Device.Vehicle.BatteryVoltageAlarmLevel.Value;
								dts.SetBatteryAlarmThreshold(deviceID, command.CommandID, null, oldValue);
								break;
							case DeviceCommandType.DisableBatteryAlarm:
								dts.SetBatteryAlarmThreshold(deviceID, command.CommandID, null, 1);
								break;
							case DeviceCommandType.EnableFeatureSet:
								//TODO: check this to make sure the right crap is getting programmed in the units when the devices have multiple subscriptions
								dts.EnableFeatureSet(deviceID, command.CommandID, null, ((EnableFeatureSet)command).FeatureSet.GetFeatureFlags());
								break;
							case DeviceCommandType.EnableCircularGeofence:
							case DeviceCommandType.EnableRectangularGeofence:
							case DeviceCommandType.EnablePolyGeofence:
								dts.SetGeofence(deviceID, ((EnableFence)command).CreateDeviceCommand(), command.CommandID, null);
								break;
							case DeviceCommandType.DisableCircularGeofence:
							case DeviceCommandType.DisableRectangularGeofence:
							case DeviceCommandType.DisablePolyGeofence:
								dts.DisableGeofence(deviceID, ((FenceCommand)command).FenceIndex, command.CommandID, null);
								break;
							case DeviceCommandType.DisableSpeedThreshold:
								dts.DisableSpeedAlert(deviceID, command.CommandID, null);
								break;
							case DeviceCommandType.EnableETD:
								dts.SetEarlyTheftDetection(deviceID, true, command.CommandID, null);
								break;
							case DeviceCommandType.DisableETD:
								dts.SetEarlyTheftDetection(deviceID, false, command.CommandID, null);
								break;
							default:
								throw new FaultException<GeneralFault>(new GeneralFault
								{
									FaultType = GeneralFaultType.InternalServerError,
									AdditionalInfo = "Something bad happened, please notify Inilex Customer Support"
								});
						}
					}
				}
			}
			catch (Exception e)
			{
				throw new FaultException<GeneralFault>(new GeneralFault
				{
					FaultType = GeneralFaultType.Other,
					AdditionalInfo = string.Format("exception thrown in sendtodts: {0}", e.Message)
				});
			}
		}

		//public static DeviceCommand SendFence(this VehicleManagement.Data.GeoFence fence)
		//{
		//  using (AccountingClient ac = new AccountingClient())
		//  {
		//    using (DeviceClient client = new DeviceClient(fence.Vehicle.Device.NetworkID))
		//    {
		//      var did = new DeviceIdentification()
		//      {
		//        DeviceID = fence.Vehicle.Device.DeviceID,
		//        ServiceID = fence.Vehicle.Device.NetworkID
		//      };
		//      //create and store the command
		//      DeviceCommand dc = new FenceCommand()
		//      {
		//        CommandID = new Guid(),
		//        CommandStatus = CommandStatus.InProgress,
		//        CommandType = fence.Enabled ? DeviceCommandType.EnableQuickFence : DeviceCommandType.DisableQuickFence,
		//        Device = fence.Vehicle.Device,
		//        ReceivedTime = null,
		//        SentTime = DateTime.UtcNow,
		//      };
		//      //db.DeviceCommands.InsertOnSubmit(dc);
		//      client.SetEarlyTheftDetection(did, true, dc.CommandID, DateTime.UtcNow.AddHours(1));
		//      return dc;
		//    }
		//  }
		//}
		public static bool SupportsFenceType(this VehicleManagement.Data.Device device, DataTypes.Geofence fence)
		{
			switch (fence.Geometry)
			{
				case GeofenceGeometryType.Circular:
					return (device.DeviceType.SupportedFenceTypes & SupportedFenceType.Circular) == SupportedFenceType.Circular;
				case GeofenceGeometryType.Rectangular:
					return (device.DeviceType.SupportedFenceTypes & SupportedFenceType.Rectangular) == SupportedFenceType.Rectangular;
				case GeofenceGeometryType.Polygon:
					return (device.DeviceType.SupportedFenceTypes & SupportedFenceType.Poly) == SupportedFenceType.Poly;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		#region to InilexVehicleManagementTypes

		public static CreditCardType ToDb(this CCTypes cardType)
		{
			switch (cardType)
			{
				case CCTypes.VISA:
					return CreditCardType.Visa;
				case CCTypes.AmericanExpress:
					return CreditCardType.AmericanExpress;
				case CCTypes.MasterCard:
					return CreditCardType.MasterCard;
				default:
					throw new FaultException<ParameterFault>(new ParameterFault()
					{
						FaultType = ParameterFaultType.InvalidType,
						AdditionalInfo = "have a nice day"
					});

			}
		}

		public static DeviceCommand ToDb(this DataTypes.Geofence fence, Device device, ConsumerAccount account, out List<CommandPolyFencePoint> points)
		{
			//TODO: support disabled fences
			DeviceCommand retval = null;
			points = null;
			switch (fence.Geometry)
			{
				case GeofenceGeometryType.Circular:
					var cfence = (SkyLinkWCFService.DataTypes.CircularGeofence)fence;
					retval = new EnableCircularFence
					{
						CenterLatitude = cfence.Center.Lat,
						CenterLongitude = cfence.Center.Lon,
						CommandStatus = CommandStatus.InProgress,
						Description = cfence.Description,
						Device = device,
						FenceIndex = cfence.Index,
						Name = cfence.Name,
						RadiusMeters = cfence.Radius,
						ReceivedTime = null,
						SentTime = DateTime.UtcNow,
						TriggerType = cfence.TriggerType.ToDbType(),
						Account = account
					};
					break;
				case GeofenceGeometryType.Rectangular:
					var rfence = (RectangleGeofence)fence;
					retval = new EnableRectangularFence
					{
						CommandStatus = CommandStatus.InProgress,
						Description = rfence.Description,
						Device = device,
						EastLongitude = rfence.SouthEastPoint.Lon,
						FenceIndex = rfence.Index,
						Name = rfence.Name,
						NorthLatitude = rfence.NorthWestPoint.Lat,
						ReceivedTime = null,
						SentTime = DateTime.UtcNow,
						SouthLatitude = rfence.SouthEastPoint.Lat,
						TriggerType = rfence.TriggerType.ToDbType(),
						WestLongitude = rfence.NorthWestPoint.Lon,
						Account = account
					};
					break;
				case GeofenceGeometryType.Polygon:
					throw new FaultException<DeviceFault>(new DeviceFault
					{
						FaultType = DeviceFaultType.UnsupportedFenceType,
						AdditionalInfo = "Device does not support polygon fences"
					});
				default:
					throw new FaultException<GeneralFault>(new GeneralFault
					{
						FaultType = GeneralFaultType.InvalidParameter,
						AdditionalInfo = "Unknown fence type"
					});
			}
			return retval;
		}

		/// <summary>
		/// Returns the equivalent dbFenceType
		/// </summary>
		/// <param name="wcfType">Service GeoFenceType.</param>
		/// <returns></returns>
		public static VehicleManagement.Data.GeoFenceType ToDbType(this DataTypes.GeofenceGeometryType wcfType)
		{
			switch (wcfType)
			{
				case GeofenceGeometryType.Circular:
					return GeoFenceType.Circular;

				case GeofenceGeometryType.Rectangular:
					return GeoFenceType.Rectangular;

				case GeofenceGeometryType.Polygon:
					throw new NotImplementedException();

				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		/// <summary>
		/// Returns the db equivalent FenceTriggerType
		/// </summary>
		/// <param name="triggerType">Type of the trigger.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentOutOfRangeException"></exception>
		public static VehicleManagement.Data.GeoTriggerType ToDbType(this DataTypes.EFenceTriggerTypes triggerType)
		{
			switch (triggerType)
			{
				case EFenceTriggerTypes.Enter:
					return GeoTriggerType.Enter;

				case EFenceTriggerTypes.Exit:
					return GeoTriggerType.Exit;

				case EFenceTriggerTypes.Transition:
					return GeoTriggerType.EnterExit;

				default:
					throw new ArgumentOutOfRangeException();
			}
		}
		#endregion
		/// <summary>
		/// Returns the equivalent db Device Fence Type
		/// </summary>
		/// <param name="geoType">Service GeoFenceType</param>
		/// <returns></returns>
		public static VehicleManagement.Data.SupportedFenceType ToDeviceType(this DataTypes.GeofenceGeometryType geoType)
		{
			switch (geoType)
			{
				case GeofenceGeometryType.Circular:
					return SupportedFenceType.Circular;
				case GeofenceGeometryType.Rectangular:
					return SupportedFenceType.Rectangular;
				case GeofenceGeometryType.Polygon:
					throw new NotImplementedException();
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static GeoFenceDirectionTrigger ToITP(this GeoTriggerType VTData)
		{
			switch (VTData)
			{
				case GeoTriggerType.Enter:
					return GeoFenceDirectionTrigger.Entering;

				case GeoTriggerType.Exit:
					return GeoFenceDirectionTrigger.Exiting;

				case GeoTriggerType.EnterExit:
					return GeoFenceDirectionTrigger.Bidirectional;
			}

			//TODO crank an valid exception
			throw new Exception();
		}

		public static GeoFenceDirectionTrigger ToITP(this DataTypes.EFenceTriggerTypes triggerType)
		{
			switch (triggerType)
			{
				case EFenceTriggerTypes.Enter:
					return GeoFenceDirectionTrigger.Entering;
				case EFenceTriggerTypes.Exit:
					return GeoFenceDirectionTrigger.Exiting;
				case EFenceTriggerTypes.Transition:
					return GeoFenceDirectionTrigger.Bidirectional;
				default:
					throw new FaultException<GeneralFault>(new GeneralFault
					{
						FaultType = GeneralFaultType.InvalidParameter,
						AdditionalInfo = "Unknown geofence trigger type"
					});
			}
		}

		public static List<GPSPoint> ToITPList(this IEnumerable<PolyFencePoint> points)
		{
			return points.Select(z => new GPSPoint
			{
				Latitude = z.Latitude,
				Longitude = z.Longitude
			}).ToList();
		}

		public static List<GPSPoint> ToITPList(this IEnumerable<CommandPolyFencePoint> points)
		{
			return points.Select(z => new GPSPoint
			{
				Latitude = z.Latitude,
				Longitude = z.Longitude
			}).ToList();
		}

		public static ITP.Common.Types.GeoFence ToITPType(this VehicleManagement.Data.GeoFence fence)
		{
			switch (fence.FenceType)
			{
				case GeoFenceType.Circular:
					var fTyped = fence as VehicleManagement.Data.CircularFence;

					if (fTyped == null)
						throw new InvalidCastException();

					return new ITP.Common.Types.CircularGeofence
					{
						CenterPoint = new GPSPoint()
						{
							Latitude = fTyped.CenterLatitude,
							Longitude = fTyped.CenterLongitude
						},
						Enabled = fTyped.Enabled,
						Index = fTyped.FenceIndex,
						Name = fTyped.Name,
						Radius = fTyped.RadiusMeters,
						Trigger = fTyped.TriggerType.ToITP()
					};

				case GeoFenceType.Rectangular:
					throw new NotImplementedException();

				case GeoFenceType.Poly:
					throw new NotImplementedException();
				default:
					throw new IndexOutOfRangeException();
			}
		}

		/// <summary>
		/// Returns the equivalent Service GeoFenceType.
		/// </summary>
		/// <param name="dbType">Db GeoFenceType</param>
		/// <returns></returns>
		public static DataTypes.EFenceGeometery ToServiceType(this VehicleManagement.Data.GeoFenceType dbType)
		{
			var wcfFenceType = DataTypes.EFenceGeometery.Circular;

			switch (dbType)
			{
				case GeoFenceType.Circular:
					wcfFenceType = EFenceGeometery.Circular;
					break;
				case GeoFenceType.Rectangular:
					wcfFenceType = EFenceGeometery.Rectangle;
					break;
				case GeoFenceType.Poly:
					wcfFenceType = EFenceGeometery.Polygon;
					break;
				default:
					//TODO throw some kinda exception
					break;
			}

			return wcfFenceType;
		}

		/// <summary>
		/// Returns the Service FenceTriggerType
		/// </summary>
		/// <param name="triggerType">Type of the trigger.</param>
		/// <returns></returns>
		public static DataTypes.EFenceTriggerTypes ToServiceType(this VehicleManagement.Data.GeoTriggerType triggerType)
		{
			switch (triggerType)
			{
				case GeoTriggerType.Enter:
					return EFenceTriggerTypes.Enter;
				case GeoTriggerType.Exit:
					return EFenceTriggerTypes.Exit;
				case GeoTriggerType.EnterExit:
					return EFenceTriggerTypes.Transition;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public static IEnumerable<LocationPoint> ToWCF(this IEnumerable<Location> locations)
		{
			foreach (Location loc in locations)
			{
				yield return loc.ToWCFType();
			}
		}

		public static DataTypes.ECommandStatus ToWCF(this VehicleManagement.Data.CommandStatus status)
		{
			switch (status)
			{
				case CommandStatus.InProgress:
					return ECommandStatus.InProgress;

				case CommandStatus.Success:
					return ECommandStatus.Success;
				case CommandStatus.Failed:
					return ECommandStatus.Failed;
				case CommandStatus.Canceled:
					return ECommandStatus.Cancelled;
				case CommandStatus.DeviceNotAccessible:
					return ECommandStatus.DeviceNotAccessible;
				default:

					throw new FaultException<GeneralFault>(new GeneralFault
					{
						FaultType = GeneralFaultType.InvalidParameter,
						AdditionalInfo = "Invalid command status recieved from client,"
					});
			}
		}

		public static string GetCellProviderString(this VehicleManagement.Data.UserBase user)
		{
			if (user.CellPhoneProvider == null)
				return "None";

			return user.CellPhoneProvider.Name;
		}

		public static SkyLinkUserInformation ToWCF(this ConsumerUser user)
		{
			return new SkyLinkUserInformation
			{
				AlternateEmail = user.AlternateEmail,
				CellProvider = user.GetCellProviderString(),
				CellPhone = user.CellPhone,
				CanAddVehicles = (user.Authorization & ConsumerAuthorizations.CanAddVehicles) == ConsumerAuthorizations.CanAddVehicles,
				CanManageUsers = (user.Authorization & ConsumerAuthorizations.CanManageSubUsers) == ConsumerAuthorizations.CanManageSubUsers,
				CanRenewSubscription = (user.Authorization & ConsumerAuthorizations.CanManageSubscriptions) == ConsumerAuthorizations.CanManageSubscriptions,
				City = user.Account.City,
				SecretQuestionChoice = user.SecretQuestion != null ? user.SecretQuestion.ID : 1,
				PIN = user.PIN,
				EmailAddress = user.PrimaryEmail,
				FirstName = user.FirstName,
				Id = user.ID,
				LastName = user.LastName,
				PrimaryPhone = user.PrimaryPhone,
				PostalCode = user.Account.PostalCode,
				State = user.Account.State,
				StreetAddress1 = user.Account.StreetAddress1,
				StreetAddress2 = user.Account.StreetAddress2,
				EmailNotifications = user.EmailNotifications,
				SmsNotifications = user.SMSNotifications,
                TimeZone = user.TimeZone,
                AutoRenew = user.Account.AutoRenew
			};
		}

		public static IDictionary<int, SkyLinkUserInformation> ToWCF(this IEnumerable<ConsumerUser> users)
		{
			Dictionary<int, SkyLinkUserInformation> dict = new Dictionary<int, SkyLinkUserInformation>();
			foreach (ConsumerUser usr in users)
			{
				dict.Add(usr.ID, usr.ToWCF());
			}

			return dict;
		}

		public static T Coalesce<T>(this T first, params T[] list) where T : class
		{
			if (first != null)
				return first;

			foreach (T item in list)
			{
				if (item != null)
					return item;
			}

			return null;
		}

		/// <summary>
		/// Returns WCF Vehicle instance from VehicleManagement.Data.Vehicle.
		/// </summary>
		/// <param name="vehicle">The vehicle.</param>
		/// <returns></returns>
		/// <remarks>added DeviceSerial 02Aug11, for insurance card.</remarks>
		public static DataTypes.Vehicle ToWCF(this VehicleManagement.Data.Vehicle vehicle)
		{
            //VehicleManagement.Data.Vehicle vehicle = vehicle2;

			return new DataTypes.Vehicle
			{
				Color = vehicle.OwnerColor.Coalesce(vehicle.Color),
				Make = vehicle.OwnerMake.Coalesce(vehicle.Make),
				Model = vehicle.OwnerModel.Coalesce(vehicle.Model),
				Year = vehicle.OwnerYear == null ? vehicle.Year : vehicle.OwnerYear.Value,
				Name = vehicle.OwnerName,
				Vin = vehicle.VIN,
				isSpeedAlertSet = vehicle.SpeedAlertEnabled,
				SkyLinkPlan = vehicle.GetWCFConsumersSubscriptionPlan(),
				isQuickFenceEnabled = vehicle.IsQuickFenceSet,
				isQuickFenceViolated = (vehicle.Locations.OrderByDescending(ts => ts.TimeStamp).FirstOrDefault(lt => lt.LocationAlertType == LocationAlertType.EarlyTheftDetection) == null) ? false :
				!(vehicle.Locations.OrderByDescending(ts => ts.TimeStamp).First(lt => lt.LocationAlertType == LocationAlertType.EarlyTheftDetection).IsAcknowledged),
				isBatteryAlertEnabled = vehicle.BatteryAlertEnabled,
				BatteryAlarmLevel = vehicle.BatteryVoltageAlarmLevel,
				DeviceSerial = vehicle.Device.SerialNumber,
                LicensePlate = vehicle.LicensePlate,
                LastLocation = vehicle.Locations.OrderByDescending(ts => ts.TimeStamp).FirstOrDefault().ToWCFType()
			};
		}

        /// <summary>
        /// Converts Credit Card to a WCF-friendly object.
        /// </summary>
        /// <param name="creditCard">The credit card.</param>
        /// <returns></returns>
        public static SkyLinkWCFService.DataTypes.CreditCard ToWCF(this VehicleManagement.Data.CreditCard creditCard)
        {
            if (creditCard == null)
                return null;

            return new SkyLinkWCFService.DataTypes.CreditCard
            {
                ExpireDate = creditCard.ExpireDate,
                CreditCardType = creditCard.CreditCardType,
                Last4Digits = creditCard.Last4Digits,
                Memo = creditCard.Memo,
                Number = creditCard.Number,
                FirstName = creditCard.FirstName,
                LastName = creditCard.LastName,
                StreetAddress1 = creditCard.StreetAddress1,
                Suite = creditCard.Suite,
                City = creditCard.City,
                State = creditCard.State,
                Zip = creditCard.Zip
            };
        }


		/// <summary>
		/// Copyies the geocoded information to a new LocationPoint
		/// The data other then the address info will still need to be
		/// filled out.
		/// </summary>
		/// <param name="reverseResult">The reverse result.</param>
		/// <returns></returns>
		public static LocationPoint ToWCFPartial(this ReverseGeocodeResult reverseResult)
		{
			var locationPoint = new LocationPoint();
			locationPoint.StreetAddress = reverseResult.Address;
			locationPoint.City = reverseResult.City;
            locationPoint.State = reverseResult.State;
			locationPoint.Country = reverseResult.Country;
			locationPoint.County = reverseResult.County;
			return locationPoint;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="dealership"></param>
		/// <returns></returns>
		/// TODO Remove the STATIC URL
		public static DataTypes.DealerShip ToWCF(this VehicleManagement.Data.DealershipAccount dealership)
		{
			return new DataTypes.DealerShip
			{
				DealerId = dealership.ID,
				City = dealership.City,
				CompanyName = dealership.CompanyName,
				PhoneNumber = dealership.ConsumerPhone,
				PostalCode = dealership.PostalCode,
				StreetAddress1 = dealership.StreetAddress1,
				StreetAddress2 = dealership.StreetAddress2,
				Suite = dealership.Suite,
				State = dealership.State,
				URL = dealership.Website ?? "http://www.inilex.com"
			};
		}

		public static IEnumerable<DataTypes.EFenceGeometery> ToWCFCollection(this VehicleManagement.Data.SupportedFenceType fenceType)
		{
			var retVal = new List<DataTypes.EFenceGeometery>();

			//is circular
			if ((fenceType & SupportedFenceType.Circular) == SupportedFenceType.Circular)
				retVal.Add(EFenceGeometery.Circular);
			//is poly
			if ((fenceType & SupportedFenceType.Poly) == SupportedFenceType.Poly)
				retVal.Add(EFenceGeometery.Polygon);
			// is quick
			if ((fenceType & SupportedFenceType.Quick) == SupportedFenceType.Quick)
				retVal.Add(EFenceGeometery.Quick);
			//is rect
			if ((fenceType & SupportedFenceType.Rectangular) == SupportedFenceType.Rectangular)
				retVal.Add(EFenceGeometery.Rectangle);

			return retVal;
		}

		/// <summary>
		/// Returns Service GeoFence equivalent
		/// </summary>
		/// <param name="dbFence">The db fence.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentOutOfRangeException"></exception>
		public static DataTypes.Geofence ToWCFType(this VehicleManagement.Data.GeoFence dbFence)
		{
			switch (dbFence.FenceType)
			{
				case GeoFenceType.Circular:

					VehicleManagement.Data.CircularFence dbCircularFence = dbFence as VehicleManagement.Data.CircularFence;

					GeoPoint center = new GeoPoint()
					{
						Lat = dbCircularFence.CenterLatitude,
						Lon = dbCircularFence.CenterLongitude
					};

					DataTypes.CircularGeofence circularFence = new DataTypes.CircularGeofence();
					circularFence.TriggerType = dbFence.TriggerType.ToServiceType();
					circularFence.Index = dbFence.FenceIndex;
					circularFence.IsEnabled = dbFence.Enabled;
					circularFence.Center = center;
					circularFence.Radius = dbCircularFence.RadiusMeters;
					circularFence.Name = dbCircularFence.Name;
					circularFence.Description = dbCircularFence.Description;

					return circularFence;

				case GeoFenceType.Rectangular:

					DataTypes.RectangleGeofence geoFence = new DataTypes.RectangleGeofence();

					VehicleManagement.Data.RectangularFence dbRectangularFence = dbFence as VehicleManagement.Data.RectangularFence;

					GeoPoint NortWestPoint = new GeoPoint()
					{
						Lat = dbRectangularFence.NorthLatitude,
						Lon = dbRectangularFence.WestLongitude,
					};

					GeoPoint SouthEastPoint = new GeoPoint()
					{
						Lat = dbRectangularFence.SouthLatitude,
						Lon = dbRectangularFence.EastLongitude
					};

					DataTypes.RectangleGeofence recFence = new RectangleGeofence();
					recFence.NorthWestPoint = NortWestPoint;
					recFence.SouthEastPoint = SouthEastPoint;
					recFence.Name = dbRectangularFence.Name;
					recFence.Index = dbRectangularFence.FenceIndex;
					recFence.IsEnabled = dbRectangularFence.Enabled;
					recFence.TriggerType = dbRectangularFence.TriggerType.ToServiceType();
					recFence.Description = dbRectangularFence.Description;

					return recFence;

				case GeoFenceType.Poly:

					throw new NotImplementedException();

				default:

					throw new ArgumentOutOfRangeException();
			}
		}

        /// <summary>
        /// Returns Service maintenanceInterval equivalent
        /// </summary>
        /// <param name="dbMaintenanceInterval">The db maintenance interval.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
		[Obsolete("This feature isn't needed in find it",true)]
        public static DataTypes.MaintenanceInterval ToWCFType(this VehicleManagement.Data.MaintenanceInterval dbMaintenanceInterval)
        {
			throw new NotImplementedException();
			//DataTypes.MaintenanceInterval maintenanceInterval = new DataTypes.MaintenanceInterval();
			//maintenanceInterval.ID = dbMaintenanceInterval.ID;
			//maintenanceInterval.AccountID = dbMaintenanceInterval.AccountID;
			//maintenanceInterval.Index = dbMaintenanceInterval.IntervalIndex;
			//maintenanceInterval.Name = dbMaintenanceInterval.Name;
			//maintenanceInterval.Description = dbMaintenanceInterval.Description;
			//maintenanceInterval.Status = dbMaintenanceInterval.Status;

			//maintenanceInterval.Interval = new MaintenanceMetrics();
			//maintenanceInterval.Interval.Days = dbMaintenanceInterval.IntervalDays;
			//maintenanceInterval.Interval.Distance = dbMaintenanceInterval.IntervalDistance;
			//maintenanceInterval.Interval.Hours = dbMaintenanceInterval.IntervalHours;

			//maintenanceInterval.Threshold1 = new MaintenanceThreshold();
			//maintenanceInterval.Threshold1.Days = dbMaintenanceInterval.DaysThreshold1;
			//maintenanceInterval.Threshold1.Distance = dbMaintenanceInterval.DistanceThreshold1;
			//maintenanceInterval.Threshold1.Hours = dbMaintenanceInterval.HoursThreshold1;

			//maintenanceInterval.Threshold2 = new MaintenanceThreshold();
			//maintenanceInterval.Threshold2.Days = dbMaintenanceInterval.DaysThreshold2;
			//maintenanceInterval.Threshold2.Distance = dbMaintenanceInterval.DistanceThreshold2;
			//maintenanceInterval.Threshold2.Hours = dbMaintenanceInterval.HoursThreshold2;

			//maintenanceInterval.Threshold3 = new MaintenanceThreshold();
			
			//maintenanceInterval.Threshold3.Days = dbMaintenanceInterval.DaysThreshold3;
			//maintenanceInterval.Threshold3.Distance = dbMaintenanceInterval.DistanceThreshold3;
			//maintenanceInterval.Threshold3.Hours = dbMaintenanceInterval.HoursThreshold3;

			//return maintenanceInterval;
        }

        /// <summary>
        /// Returns DB maintenanceInterval equivalent
        /// </summary>
        /// <param name="wcfMaintenanceInterval">The service maintenance interval.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
		[Obsolete("not used in FindIT",true)]
        public static VehicleManagement.Data.MaintenanceInterval ToDBType(this DataTypes.MaintenanceInterval wcfMaintenanceInterval, VehicleManagement.Data.MaintenanceInterval maintenanceInterval)
        {
			throw new NotImplementedException();
//            VehicleManagement.Data.MaintenanceInterval maintenanceInterval = new VehicleManagement.Data.MaintenanceInterval();
			//maintenanceInterval.ID = wcfMaintenanceInterval.ID;
			//maintenanceInterval.AccountID = wcfMaintenanceInterval.AccountID;
			//maintenanceInterval.IntervalIndex = wcfMaintenanceInterval.Index;
			//maintenanceInterval.Name = wcfMaintenanceInterval.Name;
			//maintenanceInterval.Description = wcfMaintenanceInterval.Description;
			//maintenanceInterval.Status = wcfMaintenanceInterval.Status;

			//maintenanceInterval.IntervalDays = wcfMaintenanceInterval.Interval.Days;
			//maintenanceInterval.IntervalDistance = wcfMaintenanceInterval.Interval.Distance;
			//maintenanceInterval.IntervalHours = wcfMaintenanceInterval.Interval.Hours;

			//maintenanceInterval.DaysThreshold1 = wcfMaintenanceInterval.Threshold1.Days;
			//maintenanceInterval.DistanceThreshold1 = wcfMaintenanceInterval.Threshold1.Distance;
			//maintenanceInterval.HoursThreshold1 = wcfMaintenanceInterval.Threshold1.Hours;

			//maintenanceInterval.DaysThreshold2 = wcfMaintenanceInterval.Threshold2.Days;
			//maintenanceInterval.DistanceThreshold2 = wcfMaintenanceInterval.Threshold2.Distance;
			//maintenanceInterval.HoursThreshold2 = wcfMaintenanceInterval.Threshold2.Hours;

			//maintenanceInterval.DaysThreshold3 = wcfMaintenanceInterval.Threshold3.Days;
			//maintenanceInterval.DistanceThreshold3 = wcfMaintenanceInterval.Threshold3.Distance;
			//maintenanceInterval.HoursThreshold3 = wcfMaintenanceInterval.Threshold3.Hours;

			//return maintenanceInterval;
        }       

        
        /// <summary>
		/// Returns Service VehicleMaintenanceInterval equivalent
		/// </summary>
        /// <param name="dbVehicleMaintenanceInterval">The db vehicle maintenance interval.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentOutOfRangeException"></exception>
		public static DataTypes.VehicleMaintenanceInterval ToWCFType(this VehicleManagement.Data.VehicleMaintenanceInterval dbVehicleMaintenanceInterval, int userID)
		{
            DataTypes.VehicleMaintenanceInterval vehicleMaintenanceInterval = new DataTypes.VehicleMaintenanceInterval();
            vehicleMaintenanceInterval.Index = dbVehicleMaintenanceInterval.IntervalIndex;
            vehicleMaintenanceInterval.MaintenanceIntervalID = dbVehicleMaintenanceInterval.MaintenanceIntervalID;
            vehicleMaintenanceInterval.Name = dbVehicleMaintenanceInterval.MaintenanceInterval.Name;
            vehicleMaintenanceInterval.Description = dbVehicleMaintenanceInterval.MaintenanceInterval.Description;
			//not used in FindIT
            //vehicleMaintenanceInterval.Status = (VehicleMaintenanceIntervalStatus)dbVehicleMaintenanceInterval.Status;

            vehicleMaintenanceInterval.Start = new VehicleMaintenanceMetrics();
            vehicleMaintenanceInterval.Start.Date = dbVehicleMaintenanceInterval.StartDate;
            vehicleMaintenanceInterval.Start.Odometer = dbVehicleMaintenanceInterval.StartOdometer;
            vehicleMaintenanceInterval.Start.Utilization = dbVehicleMaintenanceInterval.StartUtilization;
            vehicleMaintenanceInterval.StartUser = userID;

            vehicleMaintenanceInterval.Due = new VehicleMaintenanceMetrics();
            vehicleMaintenanceInterval.Due.Date = dbVehicleMaintenanceInterval.DueDate;
            vehicleMaintenanceInterval.Due.Odometer = dbVehicleMaintenanceInterval.DueOdometer;
            vehicleMaintenanceInterval.Due.Utilization = dbVehicleMaintenanceInterval.DueUtilization;

            vehicleMaintenanceInterval.Recorded = new VehicleMaintenanceMetrics();
            vehicleMaintenanceInterval.Recorded.Date = dbVehicleMaintenanceInterval.RecordedDate;
            vehicleMaintenanceInterval.Recorded.Odometer = dbVehicleMaintenanceInterval.RecordedOdometer;
            vehicleMaintenanceInterval.Recorded.Utilization = dbVehicleMaintenanceInterval.RecordedUtilization;

            vehicleMaintenanceInterval.DuePercent = new MaintenanceMetricPercentages();
            vehicleMaintenanceInterval.DuePercent.Days = dbVehicleMaintenanceInterval.DuePercentDays;
            vehicleMaintenanceInterval.DuePercent.Distance = dbVehicleMaintenanceInterval.DuePercentOdometer;
            vehicleMaintenanceInterval.DuePercent.Utilization = dbVehicleMaintenanceInterval.DuePercentUtilization;

            return vehicleMaintenanceInterval;
        }

        /// <summary>
        /// Returns DB VehicleMaintenanceInterval equivalent
        /// </summary>
        /// <param name="wcfVehicleMaintenanceInterval">The service vehicle maintenance interval.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentOutOfRangeException"></exception>
        public static VehicleManagement.Data.VehicleMaintenanceInterval ToDBType(this DataTypes.VehicleMaintenanceInterval wcfVehicleMaintenanceInterval)
        {
            VehicleManagement.Data.VehicleMaintenanceInterval vehicleMaintenanceInterval = new VehicleManagement.Data.VehicleMaintenanceInterval();
            vehicleMaintenanceInterval.IntervalIndex = wcfVehicleMaintenanceInterval.Index;
            vehicleMaintenanceInterval.MaintenanceIntervalID = wcfVehicleMaintenanceInterval.MaintenanceIntervalID;

            return vehicleMaintenanceInterval;
        }
        
        public static DataTypes.LocationAlarmType ToWCFType(this VehicleManagement.Data.LocationAlertType alert)
		{
			switch (alert)
			{
				case LocationAlertType.None:
					return LocationAlarmType.None;

				case LocationAlertType.IgnitionOn:
					return LocationAlarmType.IgnitionOn;
				case LocationAlertType.IgnitionOff:
					return LocationAlarmType.IgnitionOff;
				case LocationAlertType.GeofenceEnter:
					return LocationAlarmType.GeofenceEnter;
				case LocationAlertType.GeofenceExit:
					return LocationAlarmType.GeofenceExit;
				case LocationAlertType.OverSpeed:
					return LocationAlarmType.OverSpeed;
				case LocationAlertType.LowBatteryLevel:
					return LocationAlarmType.LowBatteryLevel;
				case LocationAlertType.EarlyTheftDetection:
					return LocationAlarmType.EarlyTheftDetection;
				case LocationAlertType.PowerDisconnect:
					return LocationAlarmType.PowerDisconnect;
                case LocationAlertType.PowerReconnect:
                    return LocationAlarmType.PowerReconnect;
                case LocationAlertType.Idle:
                    return LocationAlarmType.Idle;

                default:

					throw new FaultException<GeneralFault>(new GeneralFault
					{
						FaultType = GeneralFaultType.InvalidParameter,
						AdditionalInfo = "LocationAlarmType out of range."
					});
			}
		}

		public static LocationPoint ToWCFType(this Location location)
		{
			if (location == null)
				return null;

			return new LocationPoint
			{
				AlarmCleared = location.IsAcknowledged,
				City = location.City,
				Country = location.Country,
				County = location.County,
				Heading = (short?)location.Heading,
				Id = location.ID,
				IgnitionOn = location.IgnitionOn,
				Lat = location.Latitude,
				Lon = location.Longitude,
				LocationAlarmIndex = location.AlertIndex,
				LocationAlarmType = location.LocationAlertType.ToWCFType(),
				Speed = location.Speed,
				State = location.State,
				StreetAddress = location.StreetAddress,
				TimeStamp = location.TimeStamp,
				Zip = location.PostalCode
			};
		}
        
        public static IEnumerable<DataTypes.EFenceGeometery> ToWCFType(this VehicleManagement.Data.DeviceType deviceType)
		{
			foreach (EFenceGeometery geo in Enum.GetValues(typeof(EFenceGeometery)))
			{
				switch (geo)
				{
					case EFenceGeometery.Circular:
						if (deviceType.SupportedFenceTypes.HasFlag(SupportedFenceType.Circular))
						{
							yield return DataTypes.EFenceGeometery.Circular;
						}
						break;
					case EFenceGeometery.Polygon:
						if (deviceType.SupportedFenceTypes.HasFlag(SupportedFenceType.Poly))
						{
							yield return DataTypes.EFenceGeometery.Circular;
						}
						break;
					case EFenceGeometery.Rectangle:
						if (deviceType.SupportedFenceTypes.HasFlag(SupportedFenceType.Rectangular))
						{
							yield return DataTypes.EFenceGeometery.Rectangle;
						}
						break;
					default:
						throw new FaultException<GeneralFault>(new GeneralFault
						{
							FaultType = GeneralFaultType.InvalidParameter,
							AdditionalInfo = "Unsupported Fence Type"
						});
				}
			}
		}

        public static void UpdateCCInfo(int? UserAccountId, BillingInfo billingInfo, VehicleManagement.Data.VehicleManagementDataContext dc)
        {
            try
            {
                // Find the account associated with this purchase
                ConsumerAccount account = dc.ConsumerAccounts.FirstOrDefault(id => id.ID == UserAccountId);
                if (account == null)
                {
                    // Account not found, so throw a wobbler.
                    throw SkyLinkWCFService.Repositories.RepositoryLive.C_USER_NOT_FOUND;
                }
                else // Found the account
                {
                    // If user has elected to store the credit card (i.e. in NetSuite), then attempt to do so.
                    if (billingInfo.StoreCC)
                    {
                        if (!account.AddCreditCard(billingInfo.CardType.ToDb(),
                            billingInfo.CCNumber,
                            billingInfo.FirstName + " " + billingInfo.LastName,
                            billingInfo.StreetAddress + "|" + billingInfo.SuiteApt + "|" + billingInfo.City + "|" + billingInfo.State + "|" + billingInfo.Zip,
                            billingInfo.CCExpirationDate))
                        {
                            // Storing credit card failed, so we won't be wanting to auto-renew.
                            // NOTE: client can detect the change in the AutoRenew flag contained in the receipt
                            //       and use this to trigger notification to the user of failure.
                            billingInfo.AutoRenew = false;
                        }
                    }
                    else
                    {
                        // User elected not to store the credit card, so delete all cards 
                        // from the account and ensure auto-renew is turned off.
                        account.DeleteCreditCards();
                        billingInfo.AutoRenew = false;
                    }

                    // Store the auto-renew preference for this account.
                    account.AutoRenew = billingInfo.AutoRenew;

                    dc.SubmitChanges();
                }
            }
            catch (ApplicationException e)
            {
                throw new FaultException<PurchaseFault>(new PurchaseFault
                {
                    FaultType = PurchaseFaultType.Other,
                    AdditionalInfo = e.Message
                }, "Update CC Info failed, see inner fault.");
            }
        }


		#endregion Methods
	}
}