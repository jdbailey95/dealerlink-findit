﻿
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System;

namespace SkyLinkWCFService.Reports
{

	public class ReceiptItem
	{
		public string ItemID
		{
			get;
			set;
		}
		public string Name
		{
			get;
			set;
		}
		public int Quantity
		{
			get;
			set;
		}
		public string Description
		{
			get;
			set;
		}
		public double UnitCost
		{
			get;
			set;
		}
	}


	public class PurchaseReceiptDataSource
	{

		/// <summary>
		/// Gets the fake receipt.
		/// </summary>
		/// <returns></returns>
		/// <author>Paul Peterson</author>
		/// <datetime>10/27/2011-1:13 PM</datetime>
		static List<PurchaseReceipt> GetFakeReceipt()
		{
			var receipts = new List<PurchaseReceipt> { new PurchaseReceipt { CardNumber = "***8375", CardType = "MasterCard", CashSaleNumber = string.Format("1YFS{0}", (DateTime.UtcNow.Ticks / 10000000)), City = "Greenville", FirstName = "Paul", LastName = "Reeder", State = "TX", StreetAddress1 = "1008 Bell Drive", StreetAddress2 = string.Empty, VIN = "ABCDEFGHIJKLMNOPQ", Zip = "75401", Items = new List<ReceiptItem> { new ReceiptItem { Description = "SkyLink Advantage contains everything you need to do all kinds of stuff with your car", ItemID = "SL32N", Name = "SkyLink Advantage 1 Year", Quantity = 1, UnitCost = 139 } } } };

			return receipts;
		}


		/// <summary>
		/// Gets the receipts.
		/// </summary>
		/// <returns></returns>
		/// <author>Paul Peterson</author>
		/// <datetime>10/27/2011-1:13 PM</datetime>
		public List<PurchaseReceipt> GetReceipts()
		{
			//TODO have this return the PurchaseReceipt from the session
			if (HttpContext.Current == null || HttpContext.Current.Session["receipt"] == null)
			{
				return GetFakeReceipt();
			}

			return new List<PurchaseReceipt> { ((PurchaseReceipt)HttpContext.Current.Session["receipt"]) };

		}

		/// <summary>
		/// Gets the items.
		/// </summary>
		/// <returns></returns>
		/// <author>Paul Peterson</author>
		/// <datetime>10/27/2011-1:14 PM</datetime>
		public List<ReceiptItem> GetItems()
		{
			if (HttpContext.Current == null || HttpContext.Current.Session["receipt"] == null)
			{
				return GetFakeReceipt().First().Items;
			}

			var receipt = (PurchaseReceipt)HttpContext.Current.Session["receipt"];

			return receipt.Items;

		}



	}

	/// <summary>
	/// Receipt for a purchased item(s)
	/// </summary>
	/// <author>Paul Peterson</author>
	/// <datetime>10/27/2011-1:14 PM</datetime>
	public class PurchaseReceipt
	{

		internal PurchaseReceipt()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PurchaseReceipt"/> class.
		/// </summary>
		/// <param name="receipt">The receipt.</param>
		/// <param name="vin">The vin.</param>
		/// <author>Paul Peterson</author>
		/// <datetime>10/27/2011-1:14 PM</datetime>
		public PurchaseReceipt(DataTypes.PurchaseReceipt receipt, string vin)
		{
			FirstName = receipt.BillingInfo.FirstName;
			LastName = receipt.BillingInfo.LastName;
			StreetAddress1 = receipt.BillingInfo.StreetAddress;
			//StreetAddy2
			City = receipt.BillingInfo.City;
			State = receipt.BillingInfo.State;
			Zip = receipt.BillingInfo.Zip;

			if (string.IsNullOrEmpty(receipt.BillingInfo.CCNumber.Trim()) == false)
			{
				CardNumber = new string(Enumerable.Repeat('*', receipt.BillingInfo.CCNumber.Length - 4).ToArray())
					+ receipt.BillingInfo.CCNumber.Substring(receipt.BillingInfo.CCNumber.Length - 4);

				CardType = receipt.BillingInfo.CardType.ToString();
			}
			else
			{
				CardNumber = "N/A";
				CardType = "N/A";
			}


			CashSaleNumber = receipt.confirmationNumber;
			VIN = vin;
			StreetAddress2 = receipt.BillingInfo.SuiteApt;

			Items = new List<ReceiptItem>();
			foreach (var item in receipt.Items.OrderBy(x => x.OrdinalNumber))
			{
				Items.Add(new ReceiptItem
				{
					Description = item.Description,
					ItemID = item.ID.ToString(),
					Name = item.Name,
					Quantity = 1,//TODO this leads to a fix needed where people can buy more then one of something
					UnitCost = Convert.ToDouble(item.Price)

				});
			}

			HttpContext.Current.Session["receipt"] = this;
		}

		public string FirstName
		{
			get;
			set;
		}

		public string LastName
		{
			get;
			set;
		}

		public string StreetAddress1
		{
			get;
			set;
		}

		/// <summary>
		/// Gets or sets the street address2.
		/// </summary>
		/// <value>The street address2.</value>
		/// <author>Paul Peterson</author>
		/// <datetime>10/27/2011-1:15 PM</datetime>
		/// <remarks>Actually contains the Suite/Apt #</remarks>
		public string StreetAddress2
		{
			get;
			set;
		}

		public string City
		{
			get;
			set;
		}

		public string State
		{
			get;
			set;
		}

		public string Zip
		{
			get;
			set;
		}

		public string CashSaleNumber
		{
			get;
			set;
		}

		public string CardType
		{
			get;
			set;
		}

		public string CardNumber
		{
			get;
			set;
		}

		public string VIN
		{
			get;
			set;
		}

		public List<ReceiptItem> Items
		{
			get;
			set;
		}

		public double Total
		{
			get
			{
				return Items.Sum(z => z.Quantity * z.UnitCost);
			}
		}
	}
}
