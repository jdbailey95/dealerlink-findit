﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using SkyLinkWCFService.DataTypes;
using SkyLinkWCFService.Repositories;
using VehicleManagement.Data;
using System.Configuration;
using System.Web;
using SkyLinkWCFService.DataTypes.Faults;
using VehicleManagement.Data.NetSuite;
using VehicleManagement.Data.Configuration;
using SkyLinkWCFService.helpers;

namespace SkyLinkWCFService
{
	internal class ShoppingCart
	{
		#region Statics

		private readonly static int nullAccountId;

		static ShoppingCart()
		{
			nullAccountId = -1;
		}

        private static Boolean TryGetDiscountRate(UInt16 itemId, out Decimal rate)
        {
            Decimal discountRate;
            ReadResponse response;

            rate = 0.0M;

            using (var wrapper = new NetSuiteWrapper())
            {
                try
                {
                    response = wrapper.NetSuite.get(new RecordRef
                    {
                        internalId = itemId.ToString(),
                        type = RecordType.discountItem,
                        typeSpecified = true
                    });
                }
                catch
                {
                    // TODO: Log it, don't just swallow it. But this discount / coupon code piece is a hack anyway.
                    return false;
                }

                if (response == null || response.status == null || !response.status.isSuccess || response.record == null)
                    return false;

                var discount = response.record as DiscountItem;

                if (discount == null || String.IsNullOrWhiteSpace(discount.rate))
                    return false;

                var originalRate = discount.rate.Trim();

                if (Decimal.TryParse(originalRate.TrimEnd('%'), out discountRate))
                {
                    rate = originalRate[originalRate.Length - 1].Equals('%') ? discountRate / 100 : discountRate;
                    return true;
                }

                return false;
            }
        }

		#endregion

		#region Instance Fields

		private decimal total = 0;

		private string couponCode = string.Empty;

		private bool hasExistingAdvantagePlan = false;

		//private bool couponValid = false;

		private string vin = string.Empty;

		private int userAccountID = nullAccountId;

		protected List<Product> lineItems;

		#endregion

		#region Instance Properties

		public string CouponCode
		{
			get
			{
				return couponCode;
			}
			set
			{
				couponCode = value;
                couponDiscountRate = null; // reset discount rate
			}
		}

		public bool HasExistingAdvantagePlan
		{
			get
			{
				return hasExistingAdvantagePlan;
			}

			set
			{
				hasExistingAdvantagePlan = value;
			}
		}

		public bool CouponValid
		{
			get
			{
				try
				{
					String[] codes = ((string)ConfigurationManager.AppSettings["AdvantageCouponCode"]).Split(';').Select(c => c.ToLower()).ToArray();

					if (this.couponCode != null && codes.Select(c => c.Split('=').First()).Contains(this.couponCode.ToLower()) && HasExistingAdvantagePlan == false)
					{
						return true;
					}
				}
				catch (Exception)
				{
					return false; // section missing from config
				}



				return false;

			}

		}

        public Int16? CouponDiscountItemId
        {
            get
            {
                UInt16 id;

                if (!CouponValid)
                    return null;

                var code = String.Empty;
                try
                {
                    code = ConfigurationManager.AppSettings["AdvantageCouponCode"].Split(';').SingleOrDefault(c =>
                        c.StartsWith(CouponCode + '='));
                }
                catch { }

                if (String.IsNullOrWhiteSpace(code))
                    return null;

                var codeDetails = code.Split('=');

                if (!codeDetails.Length.Equals(2))
                    return null;

                if (!UInt16.TryParse(codeDetails.Last(), out id) || id > Int16.MaxValue)
                    return null;

                return (Int16)id;
            }
        }

        private Decimal? couponDiscountRate;
        public Decimal CouponDiscountRate
        {
            get
            {
                Decimal rate;

                if (couponDiscountRate.HasValue)
                    return couponDiscountRate.Value;

                if (String.IsNullOrWhiteSpace(couponCode))
                {
                    couponDiscountRate = 0;
                    return couponDiscountRate.Value;
                }

                if (!CouponDiscountItemId.HasValue)
                {
                    couponDiscountRate = 1; // 100%
                    return couponDiscountRate.Value;
                }

                if (TryGetDiscountRate((UInt16)CouponDiscountItemId.Value, out rate))
                    couponDiscountRate = rate;
                else
                    couponDiscountRate = 0;

                return couponDiscountRate.Value;
            }
        }

		public string Vin
		{
			get
			{
				return vin;
			}
			set
			{
				vin = value;
			}
		}

		public int UserAccountId
		{
			get
			{
				return userAccountID;
			}
			set
			{
				userAccountID = value;
			}
		}

		#endregion

		#region Instance Constructor(s)


		/// <summary>
		/// Initializes a new instance of the <see cref="ShoppingCart"/> class.
		/// </summary>
		/// <param name="cart">The cart.</param>
		/// <param name="UserId">The user id.</param>
		/// <author>Paul Peterson</author>
		/// <remarks>10/31/11 added checks:</br>
		/// UserID must be present</br>
		/// User must own vehicle</br>
		/// Coupon code if valid will not be applied to vehicle with existing
		/// Advantage/Protector plan.
		/// </remarks>
		/// 
		public ShoppingCart(Cart cart, int? userAccountId)
		{
			ValidateUserAccountId(userAccountId);

            /* October 2012 coupon logic */
            // If a coupon code was entered, validate it
            couponCode = cart.Coupon;

            CouponService.Response coupon = null;
            if (couponCode != null)
            {
                try
                {
                    CouponService.CouponServiceClient client = new CouponService.CouponServiceClient();
                    coupon = client.ValidateCoupon(couponCode);
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            
            if (cart == null || cart.Items == null || cart.Items.Count() == 0 || string.IsNullOrEmpty(cart.VIN.Trim()))
			{
				throw SkyLinkWCFService.Repositories.RepositoryLive.C_INVALID_SHOPPING_CART;
			}

			lineItems = new List<Product>();

			bool alreadyHasAdvantagePlan = true;

			/*
			 * Check that the user and vehicle belong to each other
			 */
			using (var dc = new VehicleManagementDataContext())
			{
				var veh = GetVehicleForAccount(cart.VIN, UserAccountId, dc);

				HasExistingAdvantagePlan = veh.ActiveSubscriptions.Where(pn => pn.LinkSubscriptionPlan.Name.Contains("advantage")).Count() != 0;

			}

            foreach (Product prod in cart.Items.OrderBy(x => x.OrdinalNumber))
			{
                // NOTE:    Current business rule assumes that free (zero-priced) product will only be included in a transaction
                //          if a current coupon is associated with it.  Further, only one coupon per transaction, meaning only one
                //          free product can be purchase.
                //
                //          Therefore, for zero-priced items, we ensure the coupon PlanID matches the ID of the purchased plan.
                //          We should also test to ensure that the quantity is no more than 1, but since all quantities are currently
                //          assumed to be 1, then we don't need to
                //          If it passes these tests, we mark the coupon as used so that we don't apply it to a subsequent line item.
                if (prod.Price == 0)
                {
                    if (prod.ID != coupon.PlanId)
                    {
                        throw new Exception("Cannot purchase a promotional product without a valid coupon associated with it."); // TODO: Should throw a real exception, e.g. "Cannot purchase promo product without a coupon"
                    }
                    // If we ever add quantity field to the cart, then test for quantity = 1 here and throw an exception if != 1.
                }
                lineItems.Add(prod);
			}

			vin = cart.VIN;

			CalcTotal();
		}
		#endregion

		#region Private Methods
		private void CalcTotal()
		{
			total = 0;

			foreach (var arg in lineItems)
			{
				total += arg.Price;
			}
		}

		/// <summary>
		/// Validates the user account id.
		/// </summary>
		/// <param name="UserAccountId">The user account id.</param>
		/// <returns></returns>
		/// <author>Paul Peterson</author>
		/// <datetime>10/31/2011-4:18 PM</datetime>
		/// <exception cref="SkyLinkWCFService.Repositories.RepositoryLive.C_USER_NOT_FOUND"/>
		private void ValidateUserAccountId(int? UserAccountId)
		{
			if (UserAccountId.HasValue == false)
			{
				throw new FaultException<SessionFault>(new SessionFault
				{
					FaultType = SessionFaultType.NotLoggedIn,
					AdditionalInfo = "UserID not present"
				}, "UserID null");
			}

			using (var dc = new VehicleManagementDataContext())
			{
				var account = dc.ConsumerAccounts.FirstOrDefault(id => id.ID == UserAccountId);
				if (account == null)
				{
					throw SkyLinkWCFService.Repositories.RepositoryLive.C_USER_NOT_FOUND;
				}
			}

			this.UserAccountId = UserAccountId.Value;
		}

		/// <summary>
		/// Determines whether vin supplied is related to a  vehicle and owned by supplied account.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="UserAccountID">The user account ID.</param>
		/// <returns>Vehicle</returns>
		/// <author>Paul Peterson</author>
		/// <datetime>10/31/2011-3:44 PM</datetime>
		/// 
		private VehicleManagement.Data.Vehicle GetVehicleForAccount(string vin, int? UserAccountId, VehicleManagementDataContext dc)
		{
			var veh = dc.Vehicles.FirstOrDefault(x => x.VIN == vin.Trim() && x.OwnerAccountRaw.ID == UserAccountId.Value);

			if (veh == null)
			{
				throw new FaultException<VehicleFault>(new VehicleFault
				{
					FaultType = VehicleFaultType.VehicleNotOwnedByUser,
					AdditionalInformation = string.Format("VIN: does not exist or is not owned by this account.", vin)
				}, "VIN Invalid or not in curren Users account");
			}

			return veh;
		}

		#region todo code implement real coupon logic

		/// <summary>
		/// Applies the coupon.
		/// </summary>
		/// <param name="coupon">The coupon.</param>
		/// <remarks>this isn't used yet....</remarks>
		//private void ApplyCoupon(string coupon)
		//{
		//  if (coupon == null || coupon == string.Empty)
		//  {
		//    couponValid = false;
		//    return;
		//  }

		//  List<Product> discounts = new List<Product>();

		//  //TODO put real coupon mechanics on here
		//  if (coupon.ToLower() == "freestuff")
		//  {
		//    foreach (var item in lineItems)
		//    {
		//      if (item.Type == CartItemType.PromotionalItem)
		//      {
		//        Product prod = new Product();
		//        prod.Name = string.Format("{0} 10% discount", item.Name);
		//        prod.Type = CartItemType.ItemDiscount;
		//        prod.Price = item.Price * -0.1M;
		//        prod.OrdinalNumber = -1;
		//        prod.ImageUrl = string.Empty;
		//        prod.ID = -1;
		//        couponValid = true;
		//        discounts.Add(prod);
		//      }
		//    }

		//    foreach (var disc in discounts)
		//    {
		//      lineItems.Add(disc);
		//    }
		//  }
		//}
		#endregion




		#endregion

		#region Public Methods

		public SkyLinkWCFService.DataTypes.PurchaseOrder GetPurchaseOrder()
		{
			var po = new SkyLinkWCFService.DataTypes.PurchaseOrder();
			po.Items = lineItems;
			po.Total = total;
			po.Applied = CouponValid;
			po.TermsNConditions = string.Empty;

			return po;
		}

		public PurchaseReceipt SubmitPurchase(BillingInfo billingInfo)
		{
			if (total > 0 && 
				CouponValid == false &&
			(
			billingInfo.CCExpirationDate.Date < DateTime.Now.Date ||
			billingInfo.CCNumber.IsNullOrEmpty() ||
			billingInfo.City.IsNullOrEmpty() ||
			billingInfo.State.IsNullOrEmpty() ||
			billingInfo.StreetAddress.IsNullOrEmpty() ||
			billingInfo.FirstName.IsNullOrEmpty() ||
			billingInfo.LastName.IsNullOrEmpty() ||
			billingInfo.Zip.IsNullOrEmpty())
			)//if
			{
				throw RepositoryLive.C_VALIDATION_FAILURE;
			}

			//here is where the magic of taking money happens

            // We used to throw an exception if more than one item was in the cart.  
            // Now we don't - we submit all items in the cart to NetSuite in a single transaction.

			string result = string.Empty;

            // We need to build the list of subscription items being purchased.
            using (var dc = new VehicleManagement.Data.VehicleManagementDataContext())
			{
                List<int> idList = lineItems.Select(x => x.ID).ToList();
				IQueryable<SubscriptionPlanPeriod> subscriptionPeriodItems = dc.SubscriptionPlanPeriods.Where(id => idList.Contains(id.ID)).Select(s => s);

                // If we have a different number of line items to subscriptionPeriodItems then assume an
                // error in matching the plan ID to the SubscriptionPlanPeriods table.
                if (subscriptionPeriodItems.Count() != lineItems.Count)
				{
					throw new FaultException<PurchaseFault>(
						new PurchaseFault
						{
							FaultType = PurchaseFaultType.InvalidProduct,
							AdditionalInfo = "Item not found."
						}, "Subscription plan doesn't have a plan period");
				}

				var vehicle = GetVehicleForAccount(vin, UserAccountId, dc);

				if (vehicle == null)
				{
					throw new FaultException<VehicleFault>(
						new VehicleFault
						{
							FaultType = VehicleFaultType.UnknownVin,
							AdditionalInformation = "vin unknown or not associated with current account"
                        }, string.Format("Vin does not exist or is not owned by the current account. VIN:{0}", vehicle.VIN));
				}

				PurchaseCreditCard cc = null;

                // Yes, it's true that if a coupon has been applied there may not be any CC info available,
                // but that's OK.
                cc = new PurchaseCreditCard
				{
					CreditCardNumber = billingInfo.CCNumber,
					SecurityCode = billingInfo.SecurityCode,
					CreditCardType = billingInfo.CardType.ToDb(),
					ExpirationDate = billingInfo.CCExpirationDate,
					NameOnCard = string.Format("{0} {1}", billingInfo.FirstName, billingInfo.LastName),
					PostalCode = billingInfo.Zip,
					StreetAddress = billingInfo.StreetAddress
				};

				try
				{
          			//create reseller record ref
                    RecordRef dealerRef = new RecordRef
			        {
				        internalId = "6390", //Inilex reseller ID
				        type = RecordType.partner,
				        typeSpecified = true
			        };

                    // Previously, we used to only send transactions to NetSuite if they had product 
                    // in them that was to be paid for (i.e. total > 0).  However, now we do.
                    //
                    // The NetSuite transaction used to be submitted from SubscriptionPlanPeriod.purchase(), but
                    // this is not optimal since it means multiple orders would need to be submitted for multiple
                    // line items.  That logic has been moved to below, with only the logic applicable to associating
                    // plans with vehicles and setting expiration dates remaining in SubScriptionPeriodPlanPeriod.purchase()

                    // Construct and submit the sales order to NetSuite
                    using (var wrapper = new NetSuiteWrapper())
                    {
                        string transactionResult = null;
                        //send the case sale to netsuite
                        var netSuiteResult = wrapper.NetSuite.add(new CashSale
                        {
                            undepFunds = false,
                            undepFundsSpecified = true,
                            //credit card info
                            paymentMethod = new RecordRef
                            {
                                internalId = ((int)cc.CreditCardType).ToString(),
                                type = RecordType.paymentMethod,
                                typeSpecified = true
                            },
                            ccName = cc.NameOnCard,
                            ccStreet = cc.StreetAddress,
                            ccZipCode = cc.PostalCode,
                            ccExpireDate = cc.ExpirationDate,
                            ccExpireDateSpecified = true,
                            ccNumber = cc.CreditCardNumber,
                            chargeIt = (total > 0),
                            chargeItSpecified = true,
                            customForm = new RecordRef
                            {
                                internalId = VehicleManagementConfiguration.Configuration.NetSuite.CashSaleFormID.ToString()
                            },
                            discountItem = CouponDiscountItemId.HasValue ? new RecordRef
                            {
                                internalId = CouponDiscountItemId.Value.ToString(),
                                type = RecordType.discountItem,
                                typeSpecified = true
                            } : null,
                            entity = new RecordRef
                            {
                                externalId = vehicle.Owner.NetSuiteID.Value.ToString(),
                                typeSpecified = true,
                                type = RecordType.customer
                            },
                            department = new RecordRef
                            {
                                internalId = "17" //"17" is "US Sales"
                            },
                            partner = dealerRef,

                            itemList = new CashSaleItemList
                            {
                                item = subscriptionPeriodItems.Select(s =>
                                        new CashSaleItem
                                        {
                                            item = new RecordRef
                                            {
                                                internalId = s.NetSuiteID.Value.ToString(),
                                                type = RecordType.serviceSaleItem,
                                                typeSpecified = true
                                            },
                                            quantity = 1,
                                            quantitySpecified = true
                                        }
                                    ).ToArray()
                            },
                            customFieldList = new CustomFieldRef[]
						    {
							    new SelectCustomFieldRef{
								    internalId = VehicleManagementConfiguration.Configuration.NetSuite.CashSaleVehicleFieldName,
								    value = new ListOrRecordRef{
									    externalId = vehicle.VIN,
									    typeId = VehicleManagementConfiguration.Configuration.NetSuite.VehicleType
								    }
							    },
							    //this payment scenario should be changed if we ever do periodic billing
							    new SelectCustomFieldRef
							    {
								    internalId = "custbodypayment_scenario",
								    value = new ListOrRecordRef{
									    internalId="2",
									    typeId="11"
								    }
							    }
						    }
                        });

                        // check result of sale
                        if (!netSuiteResult.status.isSuccess)
                        {
                            throw new ApplicationException(netSuiteResult.status.statusDetail.First().message);
                        }
                        transactionResult = netSuiteResult.baseRef.name;
                    }

                    // Execute the purchase of each of the subscription plans in the transaction.
                    foreach (SubscriptionPlanPeriod subscriptionPeriodItem in subscriptionPeriodItems)
                    {
                        result = subscriptionPeriodItem.Purchase(vehicle);//, !CouponValid);
                    }

                    // If a coupon code was entered, invalidate it
                    if (couponCode != null)
                    {
                        try
                        {
                            CouponService.CouponServiceClient client = new CouponService.CouponServiceClient();
                            // TODO: UseCoupon (and UseCouponWithSale) do not currently return a confirmation that the coupon was actually marked as used.
                            // This should probably be revisited and handled better in this code in teh event of failure.
                            //
                            // TODO: It would make sense to use UseCouponWithSale, but the NetSuite response does not currently expose internalID.
                            // While I could spend some time on this, I think it makes sense for someone else who is more familiar with it to do
                            // this while I wrap this up for publication.  In the meantime, it means that when coupons are used, they won't be tied 
                            // to the relevant NetSuite transaction ID other than in a somewhat abstract fashion via the vehicleID.
                            client.UseCoupon(couponCode, vehicle.VehicleID);
                        }
                        catch (Exception e)
                        {
                            throw e;
                        }
                    }

                    
                    // Update the stored CC info (if appropriate)
                    /*
                     * TODO:? So we only story the cc info if there was an amount charged? Shouldn't we store it if they've chosen to save it
                     * and there was a charge. This means if we run a coupon which is for free service we can't set it to auto renew. Which is the
                     * hook you get when you get a free year of reader digest or other pubs. I think this needs to allow the storing of Cc info with a 0 dollar purchase
                     * which means we have to verify the cc is valid, though we don't charge it.
                     */
                    if (total > 0)
                    {
                        Helper.UpdateCCInfo(this.UserAccountId, billingInfo, dc);
                    }
				}
				catch (ApplicationException e)
				{
					throw new FaultException<PurchaseFault>(new PurchaseFault
					{
						FaultType = PurchaseFaultType.Other,
						AdditionalInfo = e.Message
					}, "Purchase Failed, see inner fault.");
				}
			}

			var receipt = new PurchaseReceipt();
			receipt.Items = lineItems;
			receipt.Total = total;
			receipt.Applied = CouponValid;
			receipt.TermsNConditions = "";
			receipt.confirmationNumber = result;
			//change for silverlight support
			receipt.BillingInfo = billingInfo;

			HttpContext.Current.Session.Add("receipt", new Reports.PurchaseReceipt(receipt, this.vin));

			return receipt;
		}
		#endregion

	}
}