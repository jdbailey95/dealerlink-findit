﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using VehicleManagement.Data.NetSuite;
using VehicleManagement.Data;
using VehicleManagement.Data.Configuration;

namespace SkyLinkWCFService.helpers
{

    // HACK ALERT! This class does not belong in this library. I just ripped it out of VehicleManagement.Data. How ghetto is that.

    internal class NetSuiteWrapper : IDisposable
    {
        private readonly static Semaphore sem = new Semaphore(5, 5);
        private readonly static NetSuiteService[] services = new NetSuiteService[5];
        private readonly static bool[] serviceLocks = new bool[5];

        private readonly byte serviceID;

        public NetSuiteWrapper()
        {
            sem.WaitOne();

            lock (services)
            {
                for (byte i = 0; i < 5; ++i)
                {
                    if (!serviceLocks[i])
                    {
                        serviceID = i;
                        serviceLocks[i] = true;
                        break;
                    }
                }
            }

            //connect if necessary
            if (services[serviceID] == null)
            {
                services[serviceID] = new NetSuiteService
                {
                    Url = VehicleManagementConfiguration.Configuration.NetSuite.Url,
                    passport = new Passport
                    {
                        account = VehicleManagementConfiguration.Configuration.NetSuite.Account,
                        email = VehicleManagementConfiguration.Configuration.NetSuite.Login,
                        password = VehicleManagementConfiguration.Configuration.NetSuite.Password
                    }
                };
                var response = services[serviceID].login(services[serviceID].passport);
            }
        }

        public static RecordRef GetRecordRef(Guid guid, RecordType type)
        {
            if (guid.ToString().StartsWith("00000000-0000-0000-0000-0000"))
            {
                return new RecordRef
                {
                    internalId = Convert.ToInt32(guid.ToString().Substring(29), 16).ToString(),
                    type = type,
                    typeSpecified = true
                };
            }
            return new RecordRef
            {
                externalId = guid.ToString(),
                typeSpecified = true,
                type = type
            };
        }

        public NetSuiteService NetSuite
        {
            get
            {
                return services[serviceID];
            }
        }

        ~NetSuiteWrapper()
        {
            Dispose(false);
        }

        #region IDisposable Members

        private bool disposed;

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (!disposed)
            {
                disposed = true;
                sem.Release();

                lock (services)
                {
                    serviceLocks[serviceID] = false;
                }
            }
        }

        #endregion IDisposable Members
    }
}
