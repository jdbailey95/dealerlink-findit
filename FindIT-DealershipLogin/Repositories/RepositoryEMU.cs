﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Web;
using SkyLinkWCFService.DataTypes;
using SkyLinkWCFService.DataTypes.Faults;



namespace SkyLinkWCFService.Repositories
{
    public class RepositoryEmu : IRepository
    {
        /*
         * VINs we'll use here
         * 19UUA66217A023349,19UUA66218A011977,19UUA66228A019621
         * 
         */
        private static string[] _knownVins = new string[] { "19UUA66217A023349", "19UUA66218A011977", "19UUA66228A019621" };

        private static Random _randomSeed = new Random();

        private static Vehicle veh0 = new Vehicle()
                                          {
                                              Color = "Red",
                                              Make = "Toyota",
                                              Model = "FJ Cruiser",
                                              Name = "Sea Biscuit",
                                              //RenewalDate = DateTime.Now.AddMonths(12),
                                              //SkyLinkPlan = ESkyLinkPlans.Advantage,
                                              Year = 2010,
                                              Vin = "19UUA66217A023349"

                                          };
        private static Vehicle veh1 = new Vehicle()
        {
            Color = "Pink",
            Make = "Cadillac",
            Model = "El Dorado II",
            Name = "Forget Abowt It",
            //RenewalDate = DateTime.Now.AddMonths(1),
            //SkyLinkPlan = ESkyLinkPlans.StolenVehicleRecovery,
            Year = 1987,
            Vin = "19UUA66224A007673"

        };
        private static SkyLinkUserInformation
            slUser0 = new SkyLinkUserInformation()
                          {
                              Id = 1,
                              AlternateEmail = "user0@inilex.com",

                              CellPhone = "000-000-0000",
                              CanAddVehicles = true,
                              CanManageUsers = true,
                              CanRenewSubscription = true,
                              City = "Two Gun",
                              EmailAddress = "user00@inilex.com",
                              FirstName = "Paul",
                              LastName = "Reeder",
                              PrimaryPhone = "111-111-1111",
                              PostalCode = "85224",
                              State = "Tx",
                              StreetAddress1 = "123 Smarty Lane."
                          };
        private static SkyLinkUserInformation
            slUser1 = new SkyLinkUserInformation()
            {
                Id = 2,
                AlternateEmail = "user1@inilex.com",
                CellPhone = "000-000-0000",
                CanAddVehicles = true,
                CanManageUsers = true,
                CanRenewSubscription = true,
                City = "Mesa",
                EmailAddress = "user11@inilex.com",
                FirstName = "Phil",
                LastName = "DeCarloan",
                PrimaryPhone = "555-555-555",
                PostalCode = "85224",
                State = "Az",
                StreetAddress1 = "321 Kanuck Lane."
            };

        private static void IsVinValid(string vin)
        {
            if (_knownVins.Contains(vin)) return;

            VehicleFault fault = new VehicleFault();
            fault.FaultType = VehicleFaultType.UnknownVin;
            fault.AdditionalInformation = "This VIN is not in the system.";
            throw new FaultException<VehicleFault>(fault);

        }

        private static void IsFenceIndexValid(int index)
        {
            if (index <= 2) return;


            DeviceFault fault = new DeviceFault()
            {
                FaultType = DeviceFaultType.GeofenceIndexOutOfRange,
                AdditionalInfo = "The index value is not supported by this device"
            };

            throw new FaultException<DeviceFault>(fault);


        }

        

        private static void IsDateRangeValid(DateTime startDate, DateTime endDate)
        {
            if (endDate > startDate) return;

            GeneralFault fault = new GeneralFault()
                                     {
                                         FaultType = GeneralFaultType.DateRangeInvalid,
                                         AdditionalInfo = "The date range is invalid."
                                     };
        }

        private static bool RandBool()
        {
            return (_randomSeed.NextDouble() > 0.5);
        }

        private static IEnumerable<Geofence> CreateSomeCircularGeofences()
        {
            int numFences = _randomSeed.Next(1, 2);

            List<Geofence> fences = new List<Geofence>();

            for (int x = 0; x < numFences; x++)
            {
                CircularGeofence fence = new CircularGeofence()
                                             {
                                                 Center =
                                                    new GeoPoint(33.2916 + _randomSeed.NextDouble(),
                                                                       _randomSeed.NextDouble() + -111.804),
                                                 Radius = _randomSeed.Next(1, 10),
                                                 Index = x

                                             };
                fences.Add(fence);
            }

            return fences;
        }

        private static List<GeoPoint> CreateSomeGeoPoints(int num)
        {
            // skyharbor 34.46295, -112.0392
            if (num < 1) return null;

            List<GeoPoint> pts = new List<GeoPoint>(num);

            for (var index = 0; index < num; index++)
            {
                GeoPoint pt = new GeoPoint(
                    33.4629 + _randomSeed.NextDouble() / 2,
                    -112.0392 + _randomSeed.NextDouble() / 2);

                pts.Add(pt);

            }

            return pts;


        }

        //private static List<LocationPoint>CreateSomeLocationPointsWithDateRange(List<LocationPoint> pts,
        //    DateTime startDate, DateTime endDate)
        //{
        //    if (pts.Count() < 2) return null;

        //    LocationPoint[] ptsA = pts.ToArray();

        //    ptsA.First().TimeStamp = startDate;
        //    ptsA.Last().TimeStamp = endDate;

        //    TimeSpan ts = endDate - startDate;

        //    foreach (var locationPoint in ptsA)
        //    {
        //        locationPoint.TimeStamp = DateTime.Now;
        //    }
        //}

        private static List<LocationPoint> AdjustIndexOfLocationPoints(List<LocationPoint> pts, int index)
        {
            int startingIndex = index;

            foreach (var locationPoint in pts)
            {
                locationPoint.Id = startingIndex++;
            }

            return pts;

        }

        private static List<LocationPoint> CreateSomeLocationPoints(int num)
        {
            string[] streets = new string[] { "Main", "Oak", "First", "Randolf", "McCarthy" };
            string[] streetType = new string[] { "St.", "Ave.", "Blvd." };
            string[] citys = new string[] { "Mary's Igloo", "Monkey's Eyebrow", "Two Guns", "Volcano", "Hell", "Chicago", "Moscow" };
            string[] states = new string[] { "FL", "AZ", "HI", "TX", "CA", "IL", "ME" };
            string[] countys = new string[] { "McHenry", "Cook", "Maricopa", "Clay", "Union", "Washington" };
            string[] direction = new string[] { "N", "S", "E", "W" };

            List<LocationPoint> locations = new List<LocationPoint>();

            DateTime startDate = DateTime.Now.AddMonths(-12);

            for (var c = 0; c < num; c++)
            {
                LocationPoint loc = new LocationPoint(CreateSomeGeoPoints(1).First());



                loc.StreetAddress = string.Format("{0} {1} {2} {3}",
                                                    _randomSeed.Next(1, 1000),
                                                    direction[_randomSeed.Next(0, direction.Count())],
                                                    streets[_randomSeed.Next(0, streets.Count())],
                                                    streetType[_randomSeed.Next(0, streetType.Count())]);

                loc.City = citys[_randomSeed.Next(0, citys.Count())];
                loc.State = states[_randomSeed.Next(0, states.Count())];
                loc.Zip = _randomSeed.Next(6000, 69999).ToString();

                loc.County = countys[_randomSeed.Next(0, countys.Count())];
                loc.Country = "US";
                loc.IgnitionOn = RandBool();
                loc.Heading = (short)_randomSeed.Next(0, 360);
                loc.Speed = _randomSeed.Next(0, 90);
                loc.LocationAlarmType = LocationAlarmType.None;
                loc.LocationAlarmIndex = null;
                loc.TimeStamp = startDate.AddHours(c);


                locations.Add(loc);
            }

            return locations;
        }

        private static void CreateGeofenceViolation(ref LocationPoint loc)
        {
            int randNum = -1;

            while (randNum == (int)LocationAlarmType.None || randNum == -1)
            {
                randNum = _randomSeed.Next(0, 7);
            }

            loc.LocationAlarmType = (LocationAlarmType)randNum;

            loc.LocationAlarmIndex = _randomSeed.Next(0, 3);



        }


        public DashBoardInfo GetDashboard(string vin)
        {
            

            DashBoardInfo dbInfo = new DashBoardInfo(vin);

            dbInfo.BatteryLevelSetting = GetBatteryVoltageAlarmLevel(vin);
            dbInfo.CurrentUserInfomation = GetCustomerInfo();
            dbInfo.GeoFences = GetGeofences(vin);
            dbInfo.InsuranceCard = null;
            dbInfo.IsBatteryAlarmed = RandBool();
            dbInfo.IsQuickFenceSet = IsQuickFenceSet(vin);
            dbInfo.IsQuickFenceViolated = RandBool();
            dbInfo.IsSpeedAlarmed = RandBool();
            dbInfo.LastLocation = CreateSomeLocationPoints(1).FirstOrDefault();
            dbInfo.LatestTripsSummary = GetTripsSummaries(vin,
                                                          DateTime.Now.AddDays(-7),
                                                          DateTime.Now.AddDays(-2), 5);

            dbInfo.NumberOfGeofencesSupported = GetNumberOfGeofencesSupported(vin);
            dbInfo.SupportedFenceTypes = GetSupportedGeofenceTypes(vin);

            return dbInfo;


        }

        public Guid SetQuickFence(string vin, bool enable)
        {
            IsVinValid(vin);

            return Guid.NewGuid();

        }

        public bool IsQuickFenceSet(string vin)
        {
            IsVinValid(vin);

            Random rand = new Random();

            return RandBool();
        }

        public Guid SetGeofence(string vin, int index, bool enable)
        {
            IsVinValid(vin);
            IsFenceIndexValid(index);
            return Guid.NewGuid();
        }

        public IEnumerable<Geofence> GetGeofences(string vin)
        {
            IsVinValid(vin);

            if (!RandBool()) return null;

            return CreateSomeCircularGeofences();


        }

        public Guid StoreGeofence(string vin, Geofence geofence)
        {
            return Guid.NewGuid();
        }

        public Guid RemoveGeofence(string vin, int fenceId)
        {

            IsVinValid(vin);
            IsFenceIndexValid(fenceId);

            return Guid.NewGuid();

        }

        public void AckFencingViolation(int id)
        {
            IsFenceIndexValid(id);
        }

        public int AckAllFencingViolations(string vin)
        {
            IsVinValid(vin);
            return _randomSeed.Next(1, 10);

        }

        public int AckAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            IsVinValid(vin);
            return _randomSeed.Next(1, 10);
        }

        public IEnumerable<LocationPoint> GetAllFencingViolations(string vin)
        {
            IsVinValid(vin);
            int numOfViolations = _randomSeed.Next(1, 10);

            LocationPoint[] pts = CreateSomeLocationPoints(numOfViolations).ToArray();



            for (var cnt = 0; cnt < pts.Length; cnt++)
            {
                CreateGeofenceViolation(ref pts[cnt]);
            }

            return pts;
        }

        public IEnumerable<LocationPoint> GetAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            IsVinValid(vin);

            const int numOfViolations = 2;

            LocationPoint[] pts = CreateSomeLocationPoints(numOfViolations).ToArray();



            for (var cnt = 0; cnt < pts.Length; cnt++)
            {
                CreateGeofenceViolation(ref pts[cnt]);
            }

            pts[0].TimeStamp = startDate;
            pts[1].TimeStamp = endDate;

            return pts;

        }

        public int GetNumberOfGeofencesSupported(string vin)
        {
            return _randomSeed.Next(1, 3);
        }

        public IEnumerable<EFenceTriggerTypes> GetSupportedGeofenceTypes(string vin)
        {
            IsVinValid(vin);
            return new List<EFenceTriggerTypes>() { EFenceTriggerTypes.Transition, EFenceTriggerTypes.Enter, EFenceTriggerTypes.Exit };
        }

        public Guid StartTrackingSession(string vin, TimeSpan duration)
        {
            IsVinValid(vin);

            return Guid.NewGuid();
        }

        public void EndTrackingSession(string vin)
        {
            //ok;
        }

        public Guid RequestLocationUpdate(string vin)
        {
            IsVinValid(vin);

            return Guid.NewGuid();
        }

        public LocationPoint GetLastLocation(string vin)
        {
            IsVinValid(vin);
            return CreateSomeLocationPoints(1).First();
        }

        public IEnumerable<LocationPoint> GetLocationsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            IsVinValid(vin);
            IsDateRangeValid(startDate,endDate);

            LocationPoint[] locPts = CreateSomeLocationPoints(2).ToArray();

            locPts[0].TimeStamp = startDate;
            locPts[1].TimeStamp = endDate;

            return locPts;
        }

        public IEnumerable<LocationPoint> GetLocationsSince(string vin, DateTime startDate)
        {
            IsVinValid(vin);

            List<LocationPoint> ptList = CreateSomeLocationPoints(5);

            ptList[0].TimeStamp = startDate;
            ptList[1].TimeStamp = startDate.AddHours(1);
            ptList[2].TimeStamp = startDate.AddHours(1);
            ptList[3].TimeStamp = startDate.AddHours(1);
            ptList[4].TimeStamp = startDate.AddHours(1);

            return ptList;

        }

        public IEnumerable<LocationPoint> GetLastNLocations(string vin, int numberOfLocations)
        {
            IsVinValid(vin);

            return CreateSomeLocationPoints(numberOfLocations);

        }

        public IEnumerable<LocationPoint> GetLocationsAfterId(string vin, int locationId)
        {
            IsVinValid(vin);

            List<LocationPoint> pts = CreateSomeLocationPoints(_randomSeed.Next(1, 20));

            pts = AdjustIndexOfLocationPoints(pts, locationId + 1);

            return pts;

        }

        public int GetSpeedAlertSetting(string vin)
        {
            IsVinValid(vin);
            return _randomSeed.Next(45, 101);

        }

        public Guid SetSpeedAlert(string vin, int value)
        {
            IsVinValid(vin);
            return Guid.NewGuid();
        }

        public void EnableSpeedAlert(string vin, bool enable)
        {
            IsVinValid(vin);

        }

        public bool IsSpeedAlertEnabled(string vin)
        {
            IsVinValid(vin);
            return RandBool();
        }

        public void ClearSpeedAlarm(int id)
        {
            return;
        }

        public void ClearAllSpeedAlarms(string vin)
        {
            IsVinValid(vin);
        }

        public void ClearSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            IsVinValid(vin);
        }

        public IEnumerable<LocationPoint> GetAllSpeedAlarms(string vin)
        {
            IsVinValid(vin);
            int numPts = _randomSeed.Next(1, 50);
            List<LocationPoint> pts = CreateSomeLocationPoints(numPts);

            foreach (var locationPoint in pts)
            {
                locationPoint.LocationAlarmType = LocationAlarmType.OverSpeed;
            }

            return pts;

        }

        public IEnumerable<LocationPoint> GetAllSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            List<LocationPoint> pts = CreateSomeLocationPoints(2);

            pts[0].TimeStamp = startDate;
            pts[1].TimeStamp = endDate;

            return pts;
        }

        public float GetBatteryVoltageAlarmLevel(string vin)
        {
            return (float)(12.0 + _randomSeed.NextDouble());
        }

        public Guid SetBatteryVoltageAlarmLevel(string vin, float level)
        {
            IsVinValid(vin);
            return Guid.NewGuid();
        }

        public IEnumerable<LocationPoint> GetAllVoltageAlarms(string vin)
        {
            IsVinValid(vin);
            List<LocationPoint> pts = CreateSomeLocationPoints(_randomSeed.Next(1, 10));

            foreach (var locationPoint in pts)
            {
                locationPoint.LocationAlarmType = LocationAlarmType.LowBatteryLevel;
            }

            return pts;
        }

        public IEnumerable<LocationPoint> GetAllVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            IsVinValid(vin);

            List<LocationPoint> pts = CreateSomeLocationPoints(2);

            pts[0].TimeStamp = startDate;
            pts[1].TimeStamp = endDate;

            pts[0].LocationAlarmType = LocationAlarmType.LowBatteryLevel;
            pts[1].LocationAlarmType = LocationAlarmType.LowBatteryLevel;

            return pts;
        }

        public void ClearAllVoltageAlarms(string vin)
        {
            IsVinValid(vin);
            return;
        }

        public void ClearVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate)
        {
            IsVinValid(vin);
            return;
        }

        public void ClearVoltageAlarm(int id)
        {
            return;
        }

        public ECommandStatus GetCommandStatus(Guid commandId)
        {
            int stat = _randomSeed.Next(0, Enum.GetNames(typeof(ECommandStatus)).Count());

            return (ECommandStatus)stat;
        }

        public SkyLinkUserInformation GetCustomerInfo()
        {
            return slUser0;
        }

        public IDictionary<int, SkyLinkUserInformation> GetAllUsers()
        {
            IDictionary<int, SkyLinkUserInformation> dict = new Dictionary<int, SkyLinkUserInformation>() { { slUser0.Id, slUser0 }, { slUser1.Id, slUser1 } };

            return dict;
        }

        public int CreateSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newPassword)
        {
            if (skyLinkUserInformation.EmailAddress == slUser0.EmailAddress
                || skyLinkUserInformation.EmailAddress == slUser0.AlternateEmail
                || skyLinkUserInformation.AlternateEmail == slUser0.EmailAddress
                || skyLinkUserInformation.AlternateEmail == slUser0.AlternateEmail)
            {
                DatabaseFault fault = new DatabaseFault()
                          {
                              FaultType = DataBaseFaultType.EmailAddressDuplication,
                              AdditionalInfo = "One of the provided email addresses already exists in the database"
                          };
                throw new FaultException<DatabaseFault>(fault);
            }

            return _randomSeed.Next(10, 100);

        }

        public void UpdateSkyLinkUser(int skyLinkUserId, SkyLinkUserInformation skyLinkUserInformation, string newpassword)
        {
            if (skyLinkUserId != slUser0.Id || skyLinkUserId != slUser1.Id)
            {
                DatabaseFault fault = new DatabaseFault()
                                          {
                                              AdditionalInfo = "Unknown User",
                                              FaultType = DataBaseFaultType.UnknownUser
                                          };

                throw new FaultException<DatabaseFault>(fault);
            }

            return;


        }

        public void UpdateCurrentSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newpassword)
        {
            return;
        }

        public IEnumerable<Vehicle> GetVehicles()
        {
            List<Vehicle> veh = new List<Vehicle>() { veh0, veh1 };

            return veh;

        }

        public IEnumerable<TripSummary> GetTripsSummaries(string vin, DateTime startDate, DateTime endDate, int maxReports)
        {
            IsVinValid(vin);
            IsDateRangeValid(startDate, endDate);

            List<LocationPoint> lp = CreateSomeLocationPoints(6);

            lp[0].TimeStamp = DateTime.Now.AddDays(-8);
            lp[1].TimeStamp = DateTime.Now.AddDays(-7);
            lp[2].TimeStamp = DateTime.Now.AddDays(-6);
            lp[3].TimeStamp = DateTime.Now.AddDays(-5);
            lp[4].TimeStamp = DateTime.Now.AddDays(-4);
            lp[5].TimeStamp = DateTime.Now.AddDays(-3);

            List<TripSummary> tripSum = new List<TripSummary>();

            tripSum.Add(new TripSummary()
            {
                TripStart = lp[0],
                TripEnd = lp[1],
                Distance = 100
            });
            tripSum.Add(new TripSummary()
            {
                TripStart = lp[2],
                TripEnd = lp[3],
                Distance = 25
            });

            tripSum.Add(new TripSummary()
            {
                TripStart = lp[4],
                TripEnd = lp[5],
                Distance = 1
            });

            return tripSum;



        }

        public TripInfo GetTrip(string vin, DateTime startDate)
        {
            List<LocationPoint> lp = CreateSomeLocationPoints(_randomSeed.Next(1,16));

            foreach (var locationPoint in lp)
            {
                locationPoint.TimeStamp = (startDate = startDate.AddHours(1));
            }

            TripInfo tInfo = new TripInfo();

            tInfo.TripLocations = lp;

            return tInfo;


        }

        public Trip GetMostRecentTrip(string vin)
        {
            IsVinValid(vin);

            List<LocationPoint> lp = CreateSomeLocationPoints(2);

            lp[1].TimeStamp = lp[0].TimeStamp.AddHours(_randomSeed.Next(1, 12));



            Trip trip = new Trip(lp[0], lp[1]);

            return trip;


        }
    }
}