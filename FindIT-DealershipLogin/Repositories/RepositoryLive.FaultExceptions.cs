﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SkyLinkWCFService.DataTypes.Faults;
using System.ServiceModel;
using SkyLinkWCFService.Services;

namespace SkyLinkWCFService.Repositories
{
	public partial class RepositoryLive : ISkyLinkService
	{
		public static readonly FaultException<GeneralFault> C_USER_NOT_FOUND = new FaultException<GeneralFault>(new GeneralFault
		{
			AdditionalInfo = "User Not Found",
			FaultType = GeneralFaultType.UnknownUser
		},"User not found");

		public static readonly FaultException<GeneralFault> C_COMMAND_NOT_FOUND = new FaultException<GeneralFault>(new GeneralFault
		{
			AdditionalInfo = "Command not found",
			FaultType = GeneralFaultType.InvalidParameter
		},"Command not found.");

		public static readonly FaultException<VehicleFault> C_DEVICE_NOT_INSTALLED = new FaultException<VehicleFault>(new VehicleFault
		{
			FaultType = VehicleFaultType.NoDevice,
			AdditionalInformation = "no device installed"
		},"Device not installed.");

		public static readonly FaultException<GeneralFault> C_INSUFFICIENT_PERMISSIONS = new FaultException<GeneralFault>(new GeneralFault
		{
			FaultType = GeneralFaultType.NotAuthorized,
			AdditionalInfo = "User has Insufficient Permissions"
		},"User has insufficient permissions.");

		public static readonly FaultException<GeneralFault> C_LOCATION_NOT_FOUND = new FaultException<GeneralFault>(new GeneralFault
		{
			FaultType = GeneralFaultType.InvalidParameter,
			AdditionalInfo = "Location not found or location type for found location incorrect"
		},"Location not found or location type for found location is incorrect.");

		public static readonly FaultException<GeneralFault> C_METHOD_NOT_IMPLEMENTED = new FaultException<GeneralFault>(new GeneralFault
		{
			FaultType = GeneralFaultType.NotImplemented,
			AdditionalInfo = "This feature has not been implemented"
		},"This feature has not been implemented.");

		public static readonly FaultException<VehicleFault> C_VEHICLE_NOT_FOUND = new FaultException<VehicleFault>(new VehicleFault
		{
			FaultType = VehicleFaultType.UnknownVin,
			AdditionalInformation = "Unknown Vehicle"
		},"Vehicle not found.");

		public static readonly FaultException<GeneralFault> C_INVALID_PARAMETER_COMBINATION = new FaultException<GeneralFault>(new GeneralFault
		{
			FaultType = GeneralFaultType.InvalidParameterCombination,
			AdditionalInfo = "The supplied parameters are invalid."

		},"Invalid parameter combination.");

		public static readonly FaultException<GeneralFault> C_VALIDATION_FAILURE = new FaultException<GeneralFault>(new GeneralFault
		{
			FaultType = GeneralFaultType.ValidationFailure,
			AdditionalInfo = "Something didn't look right."

		},"Validation failure.");

		public static readonly FaultException<GeneralFault> C_INVALID_SHOPPING_CART = new FaultException<GeneralFault>(new GeneralFault
		{
			FaultType = GeneralFaultType.InvalidParameterCombination,
			AdditionalInfo = "Invalid Shopping Cart"

		}, "Invalid shopping cart.");
	}
}
