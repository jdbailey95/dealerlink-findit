﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq;
using ILink.Database;
using SkyLinkWCFService.DataTypes;

namespace SkyLinkWCFService.Repositories
{
    public class Repository //: IRepository
    {
        private ILink.Database.LinkDatabaseDataContext data;

        public Repository()
        {
            data = new ILink.Database.LinkDatabaseDataContext();
        }


        /// <summary>
        /// Creates a collection of trips which occured between the given dates.
        ///     If a trip started within the dates given, and ends outside the range,
        ///     the trip will be included.
        /// </summary>
        /// <param name="vin"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="maxReports"></param>
        /// <returns></returns>
        public IEnumerable<Models.DataTypes.TripSummary> GetTripsSummaries(string vin, DateTime startDate, DateTime endDate, int maxReports)
        {
            
            maxReports = Math.Abs(maxReports);

            if (maxReports < 1)
            {
                throw new ArgumentOutOfRangeException("maxReports", "Must be greater then 0");
            }

            var trips = data.Locations.Where(
                a => a.Vehicle.VIN == vin
                && a.LocationTimestamp >= startDate
                && a.AlarmType == ILink.Database.LocationAlarmType.Ignition)
                .OrderBy(b => b.LocationTimestamp)
                .SkipWhile(c => c.Ignition == false)
                .TakeWhile(d => (d.Ignition == false || d.LocationTimestamp <= endDate)).ToList();

            int tripsCreated = 0;

            List<Models.DataTypes.Trip> tripSummaries = new List<Trip>();

            //assumes the trips collection count is a whole number.
            while (tripsCreated <= maxReports && trips.Count() > 0)
            {
                Location start = trips[0];
                Location end = trips[1];
                TripSummary trip = new TripSummary();
                
                
                trip.TripStart.TimeStamp = start.LocationTimestamp;
                trip.TripStart.IgnitionOn = start.Ignition;
                trip.TripStart.Latitude = start.Latitude;
                trip.TripStart.LocationAlarmIndex = start.AlarmIndex;
                trip.TripStart.LocationAlarmType = (Models.DataTypes.LocationAlarmType)(int)start.AlarmType;
                trip.TripStart.Longitude = start.Longitude;
                trip.TripStart.Speed = start.Speed;
                trip.TripStart.Heading = start.Heading;
                trip.TripStart.StreetAddress = start.StreetAddress;
                trip.TripStart.City = start.City;
                trip.TripStart.State = start.State;
                trip.TripStart.Zip = start.PostalCode;
                trip.TripStart.County = start.County;
                trip.TripStart.Country = start.Country;




                trip.TripEnd.TimeStamp = end.LocationTimestamp;
                trip.TripEnd.IgnitionOn = end.Ignition;
                trip.TripEnd.Latitude = end.Latitude;
                trip.TripEnd.LocationAlarmIndex = end.AlarmIndex;
                trip.TripEnd.LocationAlarmType = (Models.DataTypes.LocationAlarmType)(int)end.AlarmType;
                trip.TripEnd.Longitude = end.Longitude;
                trip.TripEnd.Speed = end.Speed;
                trip.TripEnd.Heading = end.Heading;
                trip.TripEnd.StreetAddress = end.StreetAddress;
                trip.TripEnd.City = end.City;
                trip.TripEnd.State = end.State;
                trip.TripEnd.Zip = end.PostalCode;
                trip.TripEnd.County = end.County;
                trip.TripEnd.Country = end.Country;

                trip.MaxSpeed = start.Speed > end.Speed ? start.Speed : end.Speed;
                trip.Duration = end.LocationTimestamp.Subtract(start.LocationTimestamp);

                tripSummaries.Add(trip);
                
                trips.RemoveRange(0, 2);

                tripsCreated++;
                
            }

            return tripSummaries;

                
        }


        public DashBoardInfo GetDashBoard(int userId)
        {
            throw new NotImplementedException();
        }

        public Models.DataTypes.Vehicle GetVehicle(string vin)
        {
            throw new NotImplementedException();
        }
    }
}