﻿namespace SkyLinkWCFService.Repositories.old
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using SkyLinkWCFService.DataTypes;
    using SkyLinkWCFService.Services;

    public interface IRepository
    {
        #region Methods

        //TODO renamed from Clear...
			int ClearAllFencingViolations(string vin);

        //TODO renamed from Clear...
			int ClearAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate);

        //TODO renamed from Clear...
        void AckFencingViolation(int id);

        void ClearAllSpeedAlarms(string vin);

        void ClearAllVoltageAlarms(string vin);

        void ClearSpeedAlarm(int id);

        void ClearSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

        void ClearVoltageAlarm(int id);

        void ClearVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

        int CreateSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newPassword);

        Guid EnableSpeedAlert(string vin, bool enable);

        void EndTrackingSession(string vin);

        IEnumerable<LocationPoint> GetAllFencingViolations(string vin);

        IEnumerable<LocationPoint> GetAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate);

        IEnumerable<LocationPoint> GetAllSpeedAlarms(string vin);

        IEnumerable<LocationPoint> GetAllSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

        IDictionary<int, SkyLinkUserInformation> GetAllUsers();

        IEnumerable<LocationPoint> GetAllVoltageAlarms(string vin);

        IEnumerable<LocationPoint> GetAllVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

        float? GetBatteryVoltageAlarmLevel(string vin);

        IEnumerable<string> GetCellPhoneProviders();

        ECommandStatus GetCommandStatus(Guid commandId);

        SkyLinkUserInformation GetCustomerInfo();

        DashBoardInfo GetDashboard(string vin);

        IEnumerable<Geofence> GetGeofences(string vin);

        //changed to nullable 4/19/11
        LocationPoint GetLastLocation(string vin);

        IEnumerable<LocationPoint> GetLastNLocations(string vin, int numberOfLocations);

        IEnumerable<LocationPoint> GetLocationsAfterId(string vin, int locationId);

        IEnumerable<LocationPoint> GetLocationsByDate(string vin, DateTime startDate, DateTime endDate);

        IEnumerable<LocationPoint> GetLocationsSince(string vin, DateTime startDate);

        Trip GetMostRecentTrip(string vin);

        int GetNumberOfGeofencesSupported(string vin);

        int? GetSpeedAlertSetting(string vin);

        IEnumerable<EFenceGeometery> GetSupportedGeofenceTypes(string vin);

        TripInfo GetTrip(string vin, DateTime startDate);

        IEnumerable<TripSummary> GetTripsSummaries(string vin, DateTime startDate, DateTime endDate, int maxReports);

        IEnumerable<Vehicle> GetVehicles();

				LocationPoint GetMostRecentLocationByType(string vin, LocationAlarmType locationType);

        bool IsQuickFenceSet(string vin);

        bool IsSpeedAlertEnabled(string vin);

        Guid RemoveGeofence(string vin, int fenceId);

        Guid RequestLocationUpdate(string vin);

        Guid SetBatteryVoltageAlarmLevel(string vin, float level);

        Guid SetGeofence(string vin, int index, bool enable);

        Guid SetQuickFence(string vin, bool enable);

        Guid SetSpeedAlert(string vin, int value);

        Guid StartTrackingSession(string vin, TimeSpan duration);

        Guid StoreGeofence(string vin, Geofence geofence);

				LocationPoint GeoCode(string address, string city, string state, string country);

    		LocationPoint ReverseGeoCode(GeoPoint geoPoint);

        void UpdateCurrentSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newpassword, string oldpassword);

        #endregion Methods
    }
}