﻿using SkyLinkWCFService.DataTypes.Faults;
using System.Collections.Generic;
using System.ServiceModel;

namespace SkyLinkWCFService.Services
{
	public partial interface ISkyLinkService
	{
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		IEnumerable<SkyLinkWCFService.DataTypes.ConsumerFaq> GetConsumerFaqs();

		[OperationContract]
		bool IsCouponValid(string coupon);

	}
}