﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.Services.Faults
{
    [DataContract]
    public class DatabaseFault
    {
        public DataBaseFaultType FaultType { get; set; }

        public string AdditionalInfo { get; set; }
    }

    [DataContract]
    public enum DataBaseFaultType
    {
        [EnumMember]
        RecordNotFound = 1,

        [EnumMember]
        EmailAddressDuplication = 2,

        [EnumMember]
        UnknownUser = 3
    }
}