﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.Services.Faults
{
	[DataContract]
	public class GeneralFault
	{
		public GeneralFaultType FaultType { get; set; }

		public string AdditionalInfo { get; set; }
	}

	[DataContract]
	public enum GeneralFaultType
	{
		[EnumMember]
		DateRangeInvalid = 1,
		[EnumMember]
		UnknownUser = 2

	}
}