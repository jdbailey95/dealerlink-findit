﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace SkyLinkWCFService.Services.Faults
{
    [DataContract]
    public class DeviceFault
    {
        [DataMember]
        public DeviceFaultType FaultType { get; set; }

        [DataMember]
        public string AdditionalInfo { get; set; }
    }

    [DataContract]
    public enum DeviceFaultType
    {
        [EnumMember]
        GeofenceIndexOutOfRange =1,

        [EnumMember]
        UnsupportedFenceType
    }
}