﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace SkyLinkWCFService.Services.Faults
{
    [DataContract]
    public class VehicleFault
    {
        [DataMember]
        public VehicleFaultType FaultType { get; set; }

        [DataMember]
        public string AdditionalInformation { get; set; }
    }

    [DataContract]
    public enum VehicleFaultType
    {
        [EnumMember]
        UnknownVin = 1
        
    }
}