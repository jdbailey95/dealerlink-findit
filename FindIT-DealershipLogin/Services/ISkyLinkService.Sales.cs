﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SkyLinkWCFService.DataTypes;
using SkyLinkWCFService.DataTypes.Faults;
using System.ServiceModel;

namespace SkyLinkWCFService.Services
{
	public partial interface ISkyLinkService
	{
        [OperationContract]
        [FaultContract(typeof(GeneralFault))]
        [FaultContract(typeof(VehicleFault))]
        AllProducts GetCouponOfferings(string vin, string couponCode);

        [OperationContract]
        [FaultContract(typeof(GeneralFault))]
        [FaultContract(typeof(VehicleFault))]
        AllProducts GetProductOfferings(string vin);

		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(GeneralFault))]
		[FaultContract(typeof(VehicleFault))]
		PurchaseOrder SubmitCart(Cart cart);

		[OperationContract]
		[FaultContract(typeof(PurchaseFault))]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		PurchaseReceipt SubmitOrder(BillingInfo purchase);

        [OperationContract]
        [FaultContract(typeof(GeneralFault))]
        [FaultContract(typeof(VehicleFault))]
        IEnumerable<CreditCard> GetCCInfo();

        [OperationContract]
        [FaultContract(typeof(GeneralFault))]
        [FaultContract(typeof(PurchaseFault))]
        void UpdateCCInfo(BillingInfo billingInfo);

    }
}
