﻿namespace SkyLinkWCFService.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.ServiceModel;
    using System.Web;

    using SkyLinkWCFService.DataTypes;
    using SkyLinkWCFService.DataTypes.Faults;

    public partial interface ISkyLinkService
    {
        #region Methods

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        [FaultContract(typeof(DatabaseFault))]
        int CreateSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newPassword);

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        IDictionary<int, SkyLinkUserInformation> GetAllUsers();

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        SkyLinkUserInformation GetCustomerInfo();

        [OperationContract]
        [FaultContract(typeof(SessionFault))]
        void UpdateCurrentSkyLinkUser(SkyLinkUserInformation skyLinkUserInformation, string newpassword, string oldpassword);

		[OperationContract]
		void SubmitCustomerFeedBack(string email, string subject, string message);

		[OperationContract]
		IEnumerable<SecretQuestion> GetSecretQuestions();

        [OperationContract]
        IEnumerable<State> GetStates();

        [OperationContract]
        IEnumerable<string> GetCellPhoneProviders();

        #endregion Methods

        #region Other

        /*
         * User related methods
         */

        #endregion Other
    }
}