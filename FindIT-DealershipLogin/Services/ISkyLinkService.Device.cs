﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using SkyLinkWCFService.DataTypes;
using SkyLinkWCFService.DataTypes.Faults;

namespace SkyLinkWCFService.Services
{


	// NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ISkyLinkService" in both code and config file together.
	/// <summary>
	///
	/// </summary>
	[ServiceContract(Namespace = "http://mysky-link.com/skylink", SessionMode = SessionMode.Allowed)]

    public partial interface ISkyLinkService
	{
		#region Methods

		/// <summary>
		/// Clears all fencing violations.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>The number of violations cleared.</returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		[FaultContract(typeof(SessionFault))]
		int ClearAllFencingViolations(string vin);

		/// <summary>
		/// Clears all fencing violations by date.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		[FaultContract(typeof(SessionFault))]
		int ClearAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate);

		/// <summary>
		/// Clears all speed alarms.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		void ClearAllSpeedAlarms(string vin);

		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(DatabaseFault))]
		void ClearAllVoltageAlarms(string vin);

		/// <summary>
		/// Clears the fencing violation.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(DatabaseFault))]
		[FaultContract(typeof(SessionFault))]
		void ClearFencingViolation(int id);

		/// <summary>
		/// Clears the speed alarm.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(DatabaseFault))]
		void ClearSpeedAlarm(int id);

		/// <summary>
		/// Clears the speed alarms by date.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		void ClearSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(DatabaseFault))]
		void ClearVoltageAlarm(int id);

		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		void ClearVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

		/// <summary>
		/// Enables the speed alert.
		/// </summary>
		/// <param name="vin">vin of the vehicle.</param>
		/// <param name="enable">if set to <c>true</c> [enable].</param>
		/// <returns><c>true</c> if command recieved; otherwise, <c>false</c></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		Guid EnableSpeedAlert(string vin, bool enable);

		/// <summary>
		/// Sets the state of the battery alert.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="enable">if set to <c>true</c> [enable].</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		Guid SetBatteryAlertState(string vin, bool enable);

		/// <summary>
		/// Determines whether [is battery battery alert enabled] [the specified vin].
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>
		/// 	<c>true</c> if [is battery battery alert enabled] [the specified vin]; otherwise, <c>false</c>.
		/// </returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		bool IsBatteryAlertEnabled(string vin);

		/// <summary>
		/// Ends the tracking session.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>The GUID reference of the command sent to the device.</returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		void EndTrackingSession(string vin);

		/// <summary>
		/// Gets all fencing violations.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(SessionFault))]
		IEnumerable<LocationPoint> GetAllFencingViolations(string vin);

		/// <summary>
		/// Gets all fencing violations by date.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		[FaultContract(typeof(SessionFault))]
		IEnumerable<LocationPoint> GetAllFencingViolationsByDate(string vin, DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets events, optionally filtered by time and date.
        /// </summary>
        /// <param name="vFilter">Vehicle filters to apply to the set of vehicles to be returned.</param>
        /// <param name="eFilter">Event filter to apply to the set of vehicles to be returned.</param>
        /// <param name="lastLocation">Flag indicating whether or not to include last location in returned events.</param>
        /// <param name="userType">User type flag (HACK).</param>
        /// <returns></returns>
        /// <remarks>GO 3/20/12 added</remarks>
        [OperationContract]
        [FaultContract(typeof(VehicleFault))]
        [FaultContract(typeof(GeneralFault))]
        [FaultContract(typeof(SessionFault))]
        VehicleEventsResponse GetEvents(VehicleFilter vFilter, EventFilter eFilter, Boolean lastLocation, int userType=0);

		/// <summary>
		/// Gets all speed alarms for the specified vehicle.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		/// <seealso cref="LocationPoint"/>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		IEnumerable<LocationPoint> GetAllSpeedAlarms(string vin);

		/// <summary>
		/// Gets all speed alarms between the supplied date.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns><c>IEnumerable LocationPoint </c></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(GeneralFault))]
		IEnumerable<LocationPoint> GetAllSpeedAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

		/// <summary>
		/// Gets all voltage alarms.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		IEnumerable<LocationPoint> GetAllVoltageAlarms(string vin);

		/// <summary>
		/// Gets all voltage alarms by date.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		IEnumerable<LocationPoint> GetAllVoltageAlarmsByDate(string vin, DateTime startDate, DateTime endDate);

		/// <summary>
		/// Gets the battery voltage setting.
		/// *Note, this is not the voltage level of the battery, at this time that value is
		/// not obtainable.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>The voltage level at which an alarm will be
		/// generated as a <c>float</c></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		float? GetBatteryVoltageAlarmLevel(string vin);

		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(DatabaseFault))]
		ECommandStatus GetCommandStatus(Guid commandId);

		/// <summary>
		/// Gets the geofences for the vehicle.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(DeviceFault))]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(SessionFault))]
		IEnumerable<Geofence> GetGeofences(string vin);

		/// <summary>
		/// Gets the last location.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>The LocationPoint for the vehicles last known location.</returns>
		/// <seealso cref="LocationPoint"/>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		LocationPoint GetLastLocation(string vin);

		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		LocationPoint GetMostRecentLocationByType(string vin, LocationAlarmType type);

		/// <summary>
		/// Gets the last N locations.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="numberOfLocations">The number of locations.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		IEnumerable<LocationPoint> GetLastNLocations(string vin, int numberOfLocations);

		/// <summary>
		/// Gets the locations after id.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="locationId">The location id.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		IEnumerable<LocationPoint> GetLocationsAfterId(string vin, int locationId);

		/// <summary>
		/// Gets the locations by date.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(GeneralFault))]
		[FaultContract(typeof(VehicleFault))]
		IEnumerable<LocationPoint> GetLocationsByDate(string vin, DateTime startDate, DateTime endDate);

		/// <summary>
		/// Gets all occuring on or after startDate.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="startDate">The start date.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		IEnumerable<LocationPoint> GetLocationsSince(string vin, DateTime startDate);

		/// <summary>
		/// Gets the number of fences supported by the device.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(SessionFault))]
		int GetNumberOfGeofencesSupported(string vin);

		/// <summary>
		/// Gets the max speed alert setting. All Vehicles will have a speed alert value.
		///
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		int? GetSpeedAlertSetting(string vin);

		/// <summary>
		/// Gets the supported fence types.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(SessionFault))]
		IEnumerable<EFenceGeometery> GetSupportedGeofenceTypes(string vin);

		/// <summary>
		/// Determines whether the quick fence is currently enabled.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>
		/// 	<c>true</c> if [is quick fence set] [the specified vin]; otherwise, <c>false</c>.
		/// </returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(SessionFault))]
		bool IsQuickFenceSet(string vin);

		/// <summary>
		/// Determines whether [speed alert] for [the specified vin].
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>
		/// 	<c>true</c> if speed alert is enabled for the specified vin; otherwise, <c>false</c>.
		/// </returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		bool IsSpeedAlertEnabled(string vin);

		/// <summary>
		/// Removes the geo fence.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="fenceId">The fence ID.</param>
		/// <returns>The GUID reference of the command sent to the device.</returns>
		[OperationContract]
		[FaultContract(typeof(DeviceFault))]
		[FaultContract(typeof(SessionFault))]
		Guid RemoveGeofence(string vin, int fenceId);

		/// <summary>
		/// Requests the location update.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <returns>The GUID reference of the command sent to the device</returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		Guid RequestLocationUpdate(string vin);

		/// <summary>
		/// Sets the battery voltage alarm level.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="level">The level.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		Guid SetBatteryVoltageAlarmLevel(string vin, float level);

		/// <summary>
		/// Enables the geofence with the specified index.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="index">The index.</param>
		/// <param name="enable">enable</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(DeviceFault))]
		[FaultContract(typeof(GeneralFault))]
		[FaultContract(typeof(SessionFault))]
		Guid SetGeofence(string vin, int index, bool enable);

		/// <summary>
		/// Sets the quick fence.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="enable">if set to <c>true</c> [enable].</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(SessionFault))]
		Guid SetQuickFence(string vin, bool enable);

		/// <summary>
		/// Sets the speed alert.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		Guid SetSpeedAlert(string vin, int value);

		/// <summary>
		/// Starts the tracking session.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="duration">The duration.</param>
		/// <returns>The GUID reference of the command sent to the device.</returns>
		[OperationContract]
		[FaultContract(typeof(SessionFault))]
		[FaultContract(typeof(VehicleFault))]
		Guid StartTrackingSession(string vin, TimeSpan duration);

		/// <summary>
		/// Sets the geo fence.
		/// </summary>
		/// <param name="vin">The vin.</param>
		/// <param name="geofence">The geo fence.</param>
		/// <returns>The GUID reference for the command sent to the device</returns>
		/// *by default newly added Geofences will be disabled. A seperate call to
		/// SetGeofence is needed if the Geofence is to be active.
		/// <seealso cref="Geofence"/>
		[OperationContract]
		[FaultContract(typeof(DeviceFault))]
		[FaultContract(typeof(VehicleFault))]
		[FaultContract(typeof(SessionFault))]
		Guid StoreGeofence(string vin, Geofence geofence);

		#endregion Methods
	}
}