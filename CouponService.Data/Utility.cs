﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CouponService.Data
{
    public static class Utility
    {
        public const string KeyCharacters = "ACDEFGHJKLMNPQRTUVWXYZ234679";

        public static string GetUniqueKey(int maxSize)
        {
            return GetUniqueKey(maxSize, KeyCharacters);
        }

        public static string GetUniqueKey(int maxSize, string characters)
        {
            if (maxSize < 2) throw new ArgumentException("maxSize must be greater then 1");

            //char[] chars = new char[62];
            var chars = characters.ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[maxSize - 1];
                crypto.GetNonZeroBytes(data);
                StringBuilder result = new StringBuilder(maxSize);
                var checkSum = 0;
                foreach (byte b in data)
                {
                    int charValue = b % (chars.Length);
                    checkSum += charValue;
                    result.Append(chars[charValue]);
                }
                result.Append(chars[checkSum % chars.Length]);
                return result.ToString();
            }
        }
    }
}
