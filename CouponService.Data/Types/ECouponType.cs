﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;


namespace CouponService.Data.Types
{
    [DataContract]
    public enum ECouponType
    {
        [EnumMember]
        [Description("Fixed Price")]
        FixedPrice,
        [Description("Fixed Discount")]
        [EnumMember]
        FixedDiscount,
        [Description("Percentage Off Price")]
        [EnumMember]
        PercentageDiscount,
        [Description("Free Service")]
        [EnumMember]
        FreeService

    }
}